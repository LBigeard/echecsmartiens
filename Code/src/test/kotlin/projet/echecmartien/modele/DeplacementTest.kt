package projet.echecmartien.modele

import org.junit.jupiter.api.Test
import projet.echecmartien.modele.Deplacement
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.exceptions.DeplacementException


import projet.echecmartien.modele.Coordonnee
import projet.echecmartien.modele.Plateau
import java.awt.Desktop
import javax.print.attribute.standard.Destination


internal class DeplacementTest {
    var origine =Coordonnee(1,1)
    var destination =  Coordonnee(2,2)
    var Notredeplacement  = Deplacement(origine, destination)
    @Test
    fun TestgetDestination() {
        assertTrue(Notredeplacement.getDestination()== destination)
    }

    @Test
    fun TestOrigine() {
        assertTrue(Notredeplacement.getOrigine() == origine)
        var origineImossible = Coordonnee(-2,-3)

        assertThrows<IllegalArgumentException>{
            Notredeplacement = Deplacement(origineImossible,destination)
        }
        }


    @Test
    fun TestHorizontal() {
        val origine = Coordonnee(2,2)
        val destination = Coordonnee(3,2)
        var deplacement = Deplacement(origine,destination)
        assertTrue(deplacement.estHorizontal())
        deplacement = Deplacement(origine, Coordonnee(2,3))
        assertFalse(deplacement.estHorizontal())
    }

    @Test
    fun TestestVertical() {
        val origine = Coordonnee(2,2)
        val destination = Coordonnee(3,2)
        var deplacement = Deplacement(origine,destination)
        assertFalse(deplacement.estVertical())
        deplacement = Deplacement(origine, Coordonnee(2,3))
        assertTrue(deplacement.estVertical())
    }

    @Test
    fun TestestDiagonal() {
        val origine = Coordonnee(2,2)
        val destination = Coordonnee(3,2)
        var deplacement = Deplacement(origine,destination)
        assertFalse(deplacement.estDiagonal())
        deplacement = Deplacement(origine, Coordonnee(2,3))
        assertFalse(deplacement.estDiagonal())
        deplacement = Deplacement(origine, Coordonnee(2,3))
        assertFalse(deplacement.estDiagonal())
        deplacement = Deplacement(origine, Coordonnee(3,3))
        assertTrue(deplacement.estDiagonal())
    }

    @Test
    fun Testlongueur() {
        //Y et X positif
        var origine = Coordonnee(2,2)
        var destination = Coordonnee(3,3)
        assertTrue(Deplacement(origine,destination).longueur() == 1 )
        //Y et X negatif
        origine = Coordonnee(2,2)
        destination = Coordonnee(1,1)
        assertTrue(Deplacement(origine,destination).longueur() == 1 )
        //Y negatif et X positif
        origine = Coordonnee(2,2)
        destination = Coordonnee(2,1)
        assertTrue(Deplacement(origine,destination).longueur() == 1 )
        // Y positif et X negatif
        origine = Coordonnee(2,2)
        destination = Coordonnee(1,3)
        assertTrue(Deplacement(origine,destination).longueur() == 1 )
    }

    @Test
    fun TestestVerticalPositif() {
        var origine = Coordonnee(2,2)
        var destination = Coordonnee(3,3)

        assertFalse(Deplacement(origine,destination).estVerticalPositif())
        origine = Coordonnee(2,2)
        destination = Coordonnee(1,3)
        assertFalse(Deplacement(origine,destination).estVerticalPositif())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertTrue(Deplacement(origine,destination).estVerticalPositif())
    }

    @Test
    fun TestestHorizontalPositif() {
        var origine = Coordonnee(2,2)
        var destination = Coordonnee(1,2)
        assertFalse(Deplacement(origine,destination).estHorizontalPositif())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertFalse(Deplacement(origine,destination).estHorizontalPositif())

        origine = Coordonnee(1,2)
        destination = Coordonnee(3,2)
        assertTrue(Deplacement(origine,destination).estHorizontalPositif())


    }

    @Test
    fun TestestDiagonalPositifXPositifY() {
        var origine = Coordonnee(2,2)
        var destination = Coordonnee(1,1)
        assertFalse(Deplacement(origine,destination).estDiagonalPositifXPositifY())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertFalse(Deplacement(origine,destination).estDiagonalPositifXPositifY())

        origine = Coordonnee(1,1)
        destination = Coordonnee(3,3)
        assertTrue(Deplacement(origine,destination).estDiagonalPositifXPositifY())
    }

    @Test
    fun TestestDiagonalNegatifXPositifY() {
        var origine = Coordonnee(1,1)
        var destination = Coordonnee(3,3)
        assertFalse(Deplacement(origine,destination).estDiagonalNegatifXPositifY())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertFalse(Deplacement(origine,destination).estDiagonalNegatifXPositifY())

        origine = Coordonnee(3,1)
        destination = Coordonnee(1,3)
        assertTrue(Deplacement(origine,destination).estDiagonalNegatifXPositifY())
    }

    @Test
    fun TestestDiagonalPositifXNegatifY() {
        var origine = Coordonnee(3,1)
        var destination = Coordonnee(1,3)
        assertFalse(Deplacement(origine,destination).estDiagonalPositifXNegatifY())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertFalse(Deplacement(origine,destination).estDiagonalPositifXNegatifY())

        origine = Coordonnee(1,3)
        destination = Coordonnee(3,1)
        assertTrue(Deplacement(origine,destination).estDiagonalPositifXNegatifY())
    }

    @Test
    fun TestestDiagonalNegatifXNegatifY() {
        var origine = Coordonnee(1,3)
        var destination = Coordonnee(3,1)
        assertFalse(Deplacement(origine,destination).estDiagonalNegatifXNegatifY())

        origine = Coordonnee(2,2)
        destination = Coordonnee(2,3)
        assertFalse(Deplacement(origine,destination).estDiagonalNegatifXNegatifY())

        origine = Coordonnee(3,3)
        destination = Coordonnee(1,1)
        assertTrue(Deplacement(origine,destination).estDiagonalNegatifXNegatifY())
    }

    @Test
    fun TestgetCheminVertical() {
        var origine = Coordonnee(1,3)
        var destination = Coordonnee(3,1)
        assertThrows<DeplacementException> { Deplacement(origine,destination).getCheminVertical() }

        origine = Coordonnee(1,1)
        destination = Coordonnee(1,3)
        var premier = Coordonnee(1,2)
        var liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminVertical())

        origine = Coordonnee(1,3)
        destination = Coordonnee(1,1)
        premier = Coordonnee(1,2)
        liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminVertical())
    }

    @Test
    fun TestgetCheminHorizontal() {
        var origine = Coordonnee(1,1)
        var destination = Coordonnee(1,3)
        assertThrows<DeplacementException> { Deplacement(origine,destination).getCheminHorizontal() }

        origine = Coordonnee(1,1)
        destination = Coordonnee(3,1)
        var premier = Coordonnee(2,1)
        var liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminHorizontal())

        origine = Coordonnee(3,1)
        destination = Coordonnee(1,1)
        premier = Coordonnee(2,1)
        liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminHorizontal())
    }

    @Test
    fun TestgetCheminDiagonal() {
        var origine = Coordonnee(1,1)
        var destination = Coordonnee(2,1)
        assertThrows<DeplacementException> { Deplacement(origine,destination).getCheminDiagonal() }

        origine = Coordonnee(1,1)
        destination = Coordonnee(3,3)
        var premier = Coordonnee(2,2)
        var liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminDiagonal())

        origine = Coordonnee(3,3)
        destination = Coordonnee(1,1)
        premier = Coordonnee(2,2)
        liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminDiagonal())

        origine = Coordonnee(1,3)
        destination = Coordonnee(3,1)
        premier = Coordonnee(2,2)
        liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminDiagonal())

        origine = Coordonnee(3,1)
        destination = Coordonnee(1,3)
        premier = Coordonnee(2,2)
        liste = mutableListOf<Coordonnee>()
        liste.add(premier)
        liste.add(destination)
        assertEquals(liste,Deplacement(origine,destination).getCheminDiagonal())
    }
}