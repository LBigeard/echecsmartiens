package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CoordonneeTest {

    @Test
    fun getX() {
        val co=Coordonnee(1,2)
        assertEquals(1,co.getX())


    }

    @Test
    fun getY() {
        val co=Coordonnee(1,2)
        assertEquals(2,co.getY())
    }

    @Test
    fun testToString() {
        val co=Coordonnee(1,2)
        assertEquals("x : 1, y : 2",co.toString())
    }
}