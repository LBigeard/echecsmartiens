package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.exceptions.DeplacementException

internal class MoyenPionTest {

    @Test
    fun getScore() {
        var pion = MoyenPion()
        var score = pion.getScore()
        assertEquals(2,score)
    }

    @Test
    fun getDeplacementtropgrand() {
        var pion = MoyenPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(1,5)
        var dep = Deplacement(coDepart,coArrive)
        assertThrows<DeplacementException> { pion.getDeplacement(dep) }
    }

    @Test
    fun getDeplacementtroppetit(){
        var pion = MoyenPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(1,2)
        var dep = Deplacement(coDepart,coArrive)
        assertThrows<DeplacementException> { pion.getDeplacement(dep) }
    }





    @Test
    fun getDeplacementhorizontal(){
        var pion = MoyenPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(2,2)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminHorizontal(), pion.getDeplacement(dep))

    }

    @Test
    fun getDeplacementvertvertical(){
        var pion = MoyenPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(1,1)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminVertical(), pion.getDeplacement(dep))

    }

    @Test
    fun getDeplacementdiagonal(){
        var pion = MoyenPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(2,3)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminDiagonal(), pion.getDeplacement(dep))

    }

}