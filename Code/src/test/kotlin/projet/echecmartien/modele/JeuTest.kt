package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.exceptions.DeplacementException

internal class JeuTest {

    @Test
    fun getNombreSansPrise(){
        val jeu = Jeu()
        assertEquals(0, jeu.getNombreSansprise())
    }

    @Test
    fun getJoueurs(){
        val jeu = Jeu()
        assertEquals(null, jeu.getJoueur1())
        assertEquals(null, jeu.getJoueur2())
    }


    @Test
    fun getCoordOrigineDeplacement() {
        val jeu = Jeu()
        assertEquals(null, jeu.getCoordOrigineDeplacement())
    }

    @Test
    fun getCoordOrigineDeplacement1() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angele"), 5)
        jeu.deplacer(2,2,3,3)
        assertEquals(Coordonnee(2,2), jeu.getCoordOrigineDeplacement()!!)
    }

    @Test
    fun getCoordDestinationDeplacement() {
        val jeu = Jeu()
        assertEquals(null,jeu.getCoordDestinationDeplacement()
        )
    }

    @Test
    fun getCoordDestinationDeplacement1() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angele"), 5)
        jeu.deplacer(2,2,3,3)
        assertEquals(Coordonnee(3,3), jeu.getCoordDestinationDeplacement())
    }

    @Test
    fun setCoordOrigineDeplacement() {
        val jeu = Jeu()
        jeu.setCoordOrigineDeplacement(Coordonnee(4,5))
        assertEquals(Coordonnee(4,5), jeu.getCoordOrigineDeplacement())
    }

    @Test
    fun setCoordDestinationDeplacement() {
        val jeu = Jeu()
        jeu.setCoordDestinationDeplacement(Coordonnee(4,5))
        assertEquals(Coordonnee(4,5), jeu.getCoordDestinationDeplacement())
    }

    @Test
    fun getJoueurCourant() {
        val jeu = Jeu()
        assertEquals(null, jeu.getJoueurCourant())
    }

    @Test
    fun getJoueurCourant1() {
        val jeu = Jeu()
        val joueur = jeu.getJoueur1()
        assertEquals(null, jeu.getJoueurCourant())
    }

    @Test
    fun getJoueurCourant2() {
        val jeu = Jeu()
        val j1 = Joueur("Lino")
        val j2 = Joueur("Angèle")
        jeu.initialiserPartie(j1, j2, 5)
        val joueur = jeu.getJoueur1()
        assertEquals(joueur, jeu.getJoueurCourant())
    }

    @Test
    fun initialiserJoueur() {
        val jeu = Jeu()
        val Lino = Joueur("Lino")
        val Angèle = Joueur("Angèle")
        jeu.initialiserPartie(Lino, Angèle, 5)
        assertEquals(Lino, jeu.getJoueur1())
        assertEquals(Angèle, jeu.getJoueur2())
    }

    @Test
    fun initialiserPartie() {
        val jeu = Jeu()
        val lino = Joueur("lino")
        val angèle = Joueur("angèle")
        val nb = 5
        val plat = jeu.getPlateau()
        jeu.initialiserPartie(lino, angèle, nb)
        assertEquals(lino, jeu.getJoueur1())
        assertEquals(angèle, jeu.getJoueur2())
        assertEquals(lino, jeu.getJoueurCourant())
        assertEquals(plat.getCases()[2][2].getJoueur(), jeu.getJoueur1())
        assertEquals(plat.getCases()[7][2].getJoueur(), jeu.getJoueur2())
    }

    @Test
    fun deplacementPossible() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("lea"), 5)
        assertEquals(true, jeu.deplacementPossible(3, 7))
        assertEquals(true, jeu.deplacementPossible(3, 5))
        assertEquals(false, jeu.deplacementPossible(0, 7))
        assertEquals(false, jeu.deplacementPossible(3, 0))
        assertThrows<DeplacementException>() { jeu.deplacementPossible(-4, 7)}
        assertThrows<DeplacementException>() { jeu.deplacementPossible(3, -8)}
        assertThrows<DeplacementException>() { jeu.deplacementPossible(6, 12)}
    }

    @Test
    fun deplacementPossible1() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angèle"), 5)
        assertTrue(jeu.deplacementPossible(2, 2))
    }

    @Test
    fun deplacementPossible2() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angèle"), 5)
        assertThrows<DeplacementException>(){jeu.deplacementPossible(5,4)}
    }

    @Test
    fun testDeplacementPossible3() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angèle"), 5)
        val codx = 0
        val cody = 0
        println(jeu.getPlateau().getCases()[0][0].estLibre())
        assertTrue(jeu.deplacementPossible(codx, cody))
    }

    @Test
    fun deplacer() {
        val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angele"), 5)
        jeu.deplacer(2,2,3,3)
        assertEquals(Coordonnee(3,3),jeu.getCoordDestinationDeplacement() )
    }

    /*
    @Test
    fun joueurVainqueur() {
        val jeu = Jeu()
        val j1 = Joueur("Lino")
        val j2 = Joueur("Angèle")
        jeu.initialiserPartie(j1, j2, 5)
        assertEquals(null, jeu.joueurVainqueur())
    }*/

    @Test
    fun arretPartie() {
         val jeu = Jeu()
        jeu.initialiserPartie(Joueur("lino"), Joueur("angele"), 5)
        assertEquals(false,jeu.arretPartie() )
    }

    @Test
    fun changeJoueurCourant() {
        val jeu = Jeu()
        val j1 = Joueur("Lino")
        val j2 = Joueur("Angèle")
        jeu.initialiserPartie(j1, j2, 5)
        jeu.changeJoueurCourant()
        assertEquals(j2, jeu.getJoueurCourant())
    }

    @Test
    fun jeu() {
        val jeu = Jeu()
        val j1 = Joueur("luc")
        val j2 = Joueur("lino")
        jeu.initialiserPartie(j1, j2, 5)
        //println(jeu.getJoueurCourant()==j1)
        jeu.deplacer(2,2, 3,3)
        //println(jeu.getJoueurCourant()==j2)
        jeu.deplacer(3, 5, 3, 3)
        //println(jeu.getJoueurCourant()==j1)
        println(jeu.getPlateau().getCases()[1][2].getPion())
        jeu.deplacer(1, 2, 2, 3)
        //println(jeu.getJoueurCourant()==j2)
        //println(jeu.getPlateau().getCases()[1][3].getPion())
        println(jeu.getPlateau().getCases()[3][1].getPion())

    }

//    @Test
//    fun testexportpartie(){
//        var jeux = Jeu()
//        var J1 =Joueur("Ziroles")
//        var J2 =Joueur("Ziro")
//        jeux.initialiserPartie(J1,J2,5)
//        var coOrigine = Coordonnee(2,2)
//        var coDestination = Coordonnee(3,3)
//        println(jeux.getJoueur1())
//
//        jeux.deplacer(coOrigine.getX(),coOrigine.getY(),coDestination.getX(),coDestination.getY())
//
//        println(jeux.getPlateau())
//        print(jeux.getPlateau().toString())
//        print(jeux.getPlateau().getCases()[coDestination.getX()][coDestination.getY()].getPion())
//        assertTrue(jeux.getPlateau().getCases()[coDestination.getX()][coDestination.getY()] != null)
//
//        //plateau.toString()
//        assertEquals(jeux.getNombreSansprise(),1)
//        coOrigine = Coordonnee(3,5)
//        coDestination = Coordonnee(3,3)
//
//        jeux.deplacer(coOrigine.getX(),coOrigine.getY(),coDestination.getX(),coDestination.getY())
//        println(jeux.getPlateau())
//        jeux.exportsave()
//        //jeux.importjson("sauvegardeEchechmartien.json")
//        assertEquals(jeux.getJoueur1()?.getPseudo(),"Ziroles")
//
//    }
}
