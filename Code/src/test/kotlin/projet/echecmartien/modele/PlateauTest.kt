package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import projet.echecmartien.librairie.GeneralData

internal class PlateauTest {

    @Test
    fun initialiser() {
        var plateau = Plateau()
        plateau.initialiser()
        assertTrue(plateau.getCases()[0][0].getPion() is GrandPion)
    }

    @Test
    fun getTailleHorizontale() {
        var plateau = Plateau()
        assertEquals(4,plateau.getTailleHorizontale())
    }

    @Test
    fun getTailleVerticale() {
        var plateau = Plateau()
        assertEquals(8,plateau.getTailleVerticale())
    }

}