package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CaseTest {

    @Test
    fun pionOccupe() {
        val case = Case()
        val pion = GrandPion()
        case.setPion(pion)
        //assertTrue(case.occupe)
    }

    @Test
    fun pionOccupe1() {
        val case = Case()
        //assertFalse(case.occupe)
        //assertEquals(case.estLibre(), case.occupe)

    }

    @Test
    fun estLibre() {
        //val case = Case()
        //assertEquals(case.occupe, case.estLibre())
    }

    @Test
    fun estLibre1() {
        val case = Case()
        assertFalse(case.estLibre())
    }

    @Test
    fun getJoueur() {
        var case = Case()
        assertEquals(null, case.getJoueur())
    }

    @Test
    fun setJoueur() {
        var case = Case()
        var joueur = Joueur("luc")
        case.setJoueur(joueur)
        assertEquals(joueur, case.getJoueur())
    }

    @Test
    fun getPion() {
        val case = Case()
        assertEquals(null, case.getPion())
    }

    @Test
    fun getPion1() {
        val case = Case()
        val Gpion = GrandPion()
        case.setPion(Gpion)
        assertEquals(Gpion, case.getPion())
    }

    @Test
    fun setPion() {
        val case = Case()
        val Gpion = GrandPion()
        case.setPion(Gpion)
        val Ppion = PetitPion()
        case.setPion(Ppion)
        assertEquals(Ppion, case.getPion())
    }

    @Test
    fun setPion1() {
        val case = Case()
        val Gpion = GrandPion()
        case.setPion(null)
        assertEquals(null, case.getPion())
    }
}