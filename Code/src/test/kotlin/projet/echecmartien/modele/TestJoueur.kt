package projet.echecmartien.modele



import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import projet.echecmartien.modele.GrandPion
import projet.echecmartien.modele.Joueur

class TestJoueur{

    private val joueur1= Joueur("toto")

    @Test
    fun testPseudo() {
        assertEquals("toto", joueur1.getPseudo())
    }
    @Test
    fun testPionMange(){
        var unpion = GrandPion()
        joueur1.ajouterPionCaptures(unpion)
        assertTrue(unpion in joueur1.getPionsCaptures())
    }
    @Test
    fun testScoreJoueur(){
        var joueur = Joueur("Ziroles")
        joueur.ajouterPionCaptures(GrandPion())
        joueur.ajouterPionCaptures(PetitPion())

        assertEquals(joueur.calculerScore() , 4)
        joueur.ajouterPionCaptures(GrandPion())
        joueur.ajouterPionCaptures(PetitPion())
        assertEquals(joueur.calculerScore() , 8)
    }

    @Test
    fun testGlobalJoueur(){
        var joueur = Joueur("Ziroles")
        joueur.ajouterPionCaptures(GrandPion())
        assertEquals(joueur.getNbPionsCaptures(), 1)

        assertEquals(joueur.calculerScore(),3)

        assertEquals(joueur.getPseudo(), "Ziroles")

        assertEquals(joueur.getPionsCaptures().size , 1)
        var  valeur = joueur.getPionsCaptures()
        assertNotEquals(valeur.toList()[0] , null )
    }




}