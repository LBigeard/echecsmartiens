package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.exceptions.DeplacementException

internal class GrandPionTest {

    @Test
    fun getScore() {
        var Gpion = GrandPion()
        assertEquals(3, Gpion.getScore())
    }

    @Test
    fun getDeplacementExeption() {
        val Gpion = GrandPion()
        val coorDist = Coordonnee(1,5)
        val coorOrig = Coordonnee(2, 6)
        val dep = Deplacement(coorOrig, coorDist)
        assertEquals(dep.getCheminDiagonal(), Gpion.getDeplacement(dep))
    }


    @Test
    fun getDeplacementdiagonal(){
        var pion = GrandPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(2,3)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminDiagonal(), pion.getDeplacement(dep))


    }

    @Test
    fun getDeplacementvertical(){
        var pion = GrandPion()
        var coDepart = Coordonnee(1,5)
        var coArrive = Coordonnee(1,2)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminVertical(), pion.getDeplacement(dep))


    }

    @Test
    fun getDeplacementhorizontal(){
        var pion = GrandPion()
        var coDepart = Coordonnee(0,5)
        var coArrive = Coordonnee(3,5)
        var dep = Deplacement(coDepart,coArrive)

        assertEquals(dep.getCheminHorizontal(), pion.getDeplacement(dep))


    }



}