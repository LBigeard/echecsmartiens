package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import projet.echecmartien.exceptions.DeplacementException

internal class PetitPionTest {

    @Test
    fun getScore() {
        var pion = PetitPion()
        var score = pion.getScore()
        assertEquals(1,score)
    }

    @Test
    fun getDeplacementtropgrand() {
        var pion = PetitPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(3,4)
        var dep = Deplacement(coDepart,coArrive)
        assertThrows<DeplacementException>{pion.getDeplacement(dep)}
    }

    @Test
    fun getDeplacementHorizontal(){
        var pion = PetitPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(2,2)
        var dep = Deplacement(coDepart,coArrive)
        assertThrows<DeplacementException>{pion.getDeplacement(dep)}
    }

    @Test
    fun getDeplacementVertical(){
        var pion = PetitPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(1,3)
        var dep = Deplacement(coDepart,coArrive)
        assertThrows<DeplacementException>{pion.getDeplacement(dep)}
    }

    @Test
    fun getDeplacementtropBon(){
        var pion = PetitPion()
        var coDepart = Coordonnee(1,2)
        var coArrive = Coordonnee(2,3)
        var dep = Deplacement(coDepart,coArrive)

        //deplacement.getCheminDiagonal()
        println(dep.getCheminDiagonal()) //deplacement
        println(pion.getDeplacement(dep)) //listcoordonnee

        assertEquals(dep.getCheminDiagonal(),pion.getDeplacement(dep)) //fait avec equals dans coordonnee
    }
}