package projet.echecmartien.modele

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class Simulation {

    @Test
    fun Simulationtest1() {
        var plateau = Plateau()
        var Joueur1 = Joueur("Romain")
        var Joueur2 = Joueur("Lucas")
        var jeux = Jeu()

        jeux.initialiserPartie(Joueur1,Joueur2,5)




        var coOrigine = Coordonnee(2,2)
        var coDestination = Coordonnee(3,3)
        println(jeux.getJoueur1())

        jeux.deplacer(coOrigine.getX(),coOrigine.getY(),coDestination.getX(),coDestination.getY())

        println(jeux.getPlateau())

        assertTrue(!plateau.getCases()[coDestination.getY()][coDestination.getX()].estLibre())
        assertTrue(jeux.getJoueurCourant() == Joueur2)
        //plateau.toString()
        //assertEquals(jeux.getNombreSansprise(),1)
        coOrigine = Coordonnee(3,5)
        coDestination = Coordonnee(3,3)

        jeux.deplacer(coOrigine.getX(),coOrigine.getY(),coDestination.getX(),coDestination.getY())

        assertTrue(!plateau.getCases()[coDestination.getY()][coDestination.getX()].estLibre())
        assertTrue(jeux.getJoueurCourant() == Joueur1)
        assertTrue( jeux.getPionArrivedezone() != null)

        println(jeux.getPlateau())



        assertTrue(Joueur2.getNbPionsCaptures()!=0)
        assertTrue(Joueur2.getNbPionsCaptures()==1)

        jeux.getPlateau()

        assertTrue(!jeux.arretPartie())

        //jeux.deplacer(coOrigine.getX(),coOrigine.getY(),coDestination.getX(),coDestination.getY())

        //assertEquals(jeux.joueurVainqueur(), Joueur1) non testé en l'état


    }


}