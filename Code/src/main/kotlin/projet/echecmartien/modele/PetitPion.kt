package projet.echecmartien.modele

import projet.echecmartien.exceptions.DeplacementException
import projet.echecmartien.librairie.GeneralData

class PetitPion : Pion(){


    override fun getScore(): Int {
        return 1
    }

    //Les petits pions se déplacent diagonalement de 1 case.

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        if (deplacement.longueur() > 1) {
            throw DeplacementException()
        }
        if(deplacement.estHorizontal()){
            throw DeplacementException()
        }
        if(deplacement.estVertical()){
            throw DeplacementException()
        }

        return deplacement.getCheminDiagonal()
    }
}