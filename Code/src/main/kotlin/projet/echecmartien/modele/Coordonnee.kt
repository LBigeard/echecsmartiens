package projet.echecmartien.modele

class Coordonnee(x : Int, y : Int ) {
    private var x : Int
    private var y : Int

    init{
       this.x = x
        this.y = y
    }

    /**
     *@return la coordonnée en x
     */
    fun getX(): Int{
        return x
    }


    /**
     *@return la coordonnée en y
     */
    fun getY(): Int{
       return y
    }


    override fun toString():String{
       return "x : $x, y : $y"
    }

    override fun equals(other: Any?): Boolean {
        if (other == null){
            return false
        }

        if (other !is Coordonnee){
            return false
        }

        return (other.getX() == this.getX() && other.getY() == this.getY())
    }
}