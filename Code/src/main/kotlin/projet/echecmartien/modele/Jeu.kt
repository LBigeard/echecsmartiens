package projet.echecmartien.modele

import com.google.gson.Gson
import projet.echecmartien.exceptions.DeplacementException
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import projet.echecmartien.librairie.GeneralData

class Jeu : InterfaceJeu {

    private var nombreCoupsSansPrise : Int
    private var nombreCoupsSansPriseMax : Int
    private var joueur1 : Joueur?
    private var joueur2 : Joueur?
    private var joueurCourant : Joueur?
    private var pion : Pion?
    private var pionArriveDeZone : Pion?
    private var pionsCaptures = arrayOf<Pion>()
    private var coordOrigine : Coordonnee?
    private var coordDest : Coordonnee?
    private var plateau : Plateau
    private var deplacement: Deplacement?
    private var emplacementpionarrivedezone : Coordonnee?


    init {
        this.nombreCoupsSansPrise = 0
        this.nombreCoupsSansPriseMax = 0
        this.joueur1 = null
        this.joueur2 = null
        this.joueurCourant = null
        this.pion = null
        this.pionArriveDeZone = null
        this.coordOrigine = null
        this.coordDest = null
        this.plateau = Plateau()
        this.deplacement = null
        this.emplacementpionarrivedezone = null
    }

    /**
     * getter
     * @return la coordonnée origine du déplacement
     */
    fun getNombreSansprise():Int{
        return nombreCoupsSansPrise
    }

    fun getPlateau() : Plateau{
        return plateau
    }

    fun getJoueur1():Joueur?{
        return joueur1
    }

    fun getPionArrivedezone() : Pion?{
        return pionArriveDeZone
    }

    fun getJoueur2():Joueur?{
        return joueur2
    }
    fun getCoordOrigineDeplacement(): Coordonnee? {
        return coordOrigine
    }

    /**
     * getter
     * @return la coordonnée destination du déplacement
     */
    fun getCoordDestinationDeplacement(): Coordonnee? {
        return coordDest
    }


    /**
     * setter
     * @param origine la coordonnée origine du déplacement
     */
    fun setCoordOrigineDeplacement(origine: Coordonnee) {
        coordOrigine = origine
    }

    /**
     * setter
     * @param destination la coordonnée destination du déplacement
     */
    fun setCoordDestinationDeplacement(destination: Coordonnee){
        coordDest = destination
    }

    /** retourne le joueur courant
     * @return le joueur courant
     */
    fun getJoueurCourant(): Joueur? {
        return joueurCourant
    }

    override fun initialiserPartie(joueur1: Joueur, joueur2: Joueur, nombreCoupsSansPriseMax: Int) {
        this.joueur1 = joueur1
        this.joueur2 = joueur2
        nombreCoupsSansPrise = 0
        var a = arretPartie()
        this.nombreCoupsSansPriseMax = nombreCoupsSansPriseMax
        a = false
        initialiserJoueur(joueur1,joueur2)
        plateau.initialiser()
    }

    /**
     * affectation des joueurs aux cases
     * @param joueur1 premier joueur
     * @param joueur2 second joueur
     */
    private fun initialiserJoueur(joueur1: Joueur, joueur2: Joueur) {
        this.joueur1 = joueur1
        this.joueur2 = joueur2
        this.joueurCourant = joueur1
        //* affectation des joueurs aux cases
        //X et Y sont inversé, [Y][X]
        for (hauteur in 0 until 4){
            for (largeur in 0 until 4){
                plateau.getCases()[hauteur][largeur].setJoueur(joueur1)
            }
        }
        for (hauteur in 4 until 8){
            for (largeur in 0 until 4){
                plateau.getCases()[hauteur][largeur].setJoueur(joueur2)
            }
        }
    }

    override fun deplacementPossible(coordOrigineX: Int, coordOrigineY: Int): Boolean {
        //verifie si la case est sur le plateau
        if (arretPartie()) return false
        this.coordOrigine = Coordonnee(coordOrigineX, coordOrigineY)
        if (coordOrigineY>7 || coordOrigineY<0 || coordOrigineX>3 || coordOrigineX<0){
            //println("case pas sur plateau")
            throw DeplacementException()
        }
        //vérifie si la case est libre
        val caseOrigine = plateau.getCases()[coordOrigineY][coordOrigineX]
        if (!caseOrigine.estLibre()){
            //println("case pas libre")
            return false
        }
        //vérifie si le pion est capturée ou s'il existe
        pion = plateau.getCases()[coordOrigineY][coordOrigineX].getPion()
        if (pion in pionsCaptures || pion == null) {
            //println("pion capturé ou existe pas")
            return false
        }
        return true
    }

    override fun deplacementPossible(coordOrigineX: Int, coordOrigineY: Int, coordDestinationX: Int, coordDestinationY: Int, joueur: Joueur?): Boolean {
        if (arretPartie()) return false
        coordOrigine = Coordonnee(coordOrigineX, coordOrigineY)
        coordDest = Coordonnee(coordDestinationX, coordDestinationY)

        //TEST DEPLACEMENT VALIDE CELON LE TYPE DU PION      AJOUT
        try{
            plateau.getCases()[coordOrigineY][coordOrigineX].getPion()?.getDeplacement(Deplacement(
                    coordOrigine!!, coordDest!!))
        }  //throw deplacement exeption si marche pas donc permet de test si le deplacement est bon par rappot au pion
        catch (e : DeplacementException){
            return false
        }

        if (coordOrigineY==coordDestinationY && coordOrigineX==coordDestinationX)return false
        // pour pas manger ton pion gros malin
        if (plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur() == plateau.getCases()[coordDestinationY][coordDestinationX].getJoueur() ){
            if(plateau.getCases()[coordDestinationY][coordDestinationX].getPion() != null) return false
        }
        this.deplacement = Deplacement(coordOrigine!!,coordDest!!)

        //print("0")

        //print(joueurCourant?.getPseudo() != plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()?.getPseudo())
        //print(plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()?.getPseudo()!=null)
        pion = plateau.getCases()[coordOrigineY][coordOrigineX].getPion()
        if (!deplacementPossible(coordOrigineX, coordOrigineY) || joueurCourant?.getPseudo() != plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()?.getPseudo() || plateau.getCases()[coordOrigineY][coordOrigineX].getPion() != pion){
            throw DeplacementException()
        }
        //print("0.5")
        // VERIFICATION SI PAS LE DERNIER A AVOIR CHANGER DE ZONE
        if (plateau.getCases()[coordOrigineY][coordOrigineX].getPion()==pionArriveDeZone){
            if (coordOrigineY>=4 && coordDestinationY <4 ){
                return false
            }else if (coordOrigineY<4 && coordDestinationY >=4){
                return false
            }
        }
        print("1")
        // VERIFICATION SI ON PASSE AU DESSU D'UN PION
        var depl = Deplacement(coordOrigine!!,coordDest!!)
        if (depl.estDiagonal()){
            print("thisdeplsD")
            var deptab = depl.getCheminDiagonal()
            for (i in deptab){
                if (plateau.getCases()[i.getY()][i.getX()].getPion()!=null) {
                    if (i == deptab[deptab.size - 1]) {
                        return plateau.getCases()[i.getY()][i.getX()].getJoueur() != plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()
                    }else{
                        return false
                    }
                }
            }
        }else if(depl.estVertical()){
            //print("thisdeplsV")
            var deptab = depl.getCheminVertical()
            for (i in deptab){
                if (plateau.getCases()[i.getY()][i.getX()].getPion()!=null){
                    if (i == deptab[deptab.size - 1]) {
                        return plateau.getCases()[i.getY()][i.getX()].getJoueur() != plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()
                    }else{
                        return false
                    }
                }
            }

        }else if(depl.estHorizontal()){
            //print("thisdeplsH")
            var deptab = depl.getCheminHorizontal()
            for (i in deptab){
                if (plateau.getCases()[i.getY()][i.getX()].getPion()!=null){
                    if (i == deptab[deptab.size-1 ]) {
                        //print(plateau.getCases()[i.getY()][i.getX()].getJoueur())
                        //print(plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur())
                        return plateau.getCases()[i.getY()][i.getX()].getJoueur() != plateau.getCases()[coordOrigineY][coordOrigineX].getJoueur()
                    }else{
                        return false
                    }
                }

            }
        }



        return true
    }

    override fun deplacer(coordOrigineX: Int, coordOrigineY: Int, coordDestinationX: Int, coordDestinationY: Int) {

        if (!deplacementPossible(coordOrigineX,coordOrigineY,coordDestinationX,coordDestinationY,joueurCourant)){
            throw DeplacementException()
        }
        coordOrigine = Coordonnee(coordOrigineX, coordOrigineY)
        coordDest = Coordonnee(coordDestinationX, coordDestinationY)

        //TEST DEPLACEMENT VALIDE CELON LE TYPE DU PION      AJOUT
        plateau.getCases()[coordOrigineY][coordOrigineX].getPion()?.getDeplacement(Deplacement(
                coordOrigine!!, coordDest!!))  //throw deplacement exeption si marche pas donc permet de test si le deplacement est bon par rappot au pion



        // AJOUT AU PION CAPTURES
        if (plateau.getCases()[coordDestinationY][coordDestinationX].estLibre()){ //inversé X et Y sinon marche pas
            joueurCourant?.ajouterPionCaptures(plateau.getCases()[coordDestinationY][coordDestinationX].getPion()!!)
            nombreCoupsSansPrise=0
            plateau.getCases()[coordDestinationY][coordDestinationX].setPion(null)
            //println("le score est de"+joueurCourant!!.calculerScore())
        }else{
            nombreCoupsSansPrise += 1
        }
        //-----------------CHANGEMENT DE ZONE
        if (coordOrigineY<=3 && coordDestinationY>3){
            emplacementpionarrivedezone= Coordonnee(coordDestinationX,coordDestinationY)
            pionArriveDeZone=plateau.getCases()[coordOrigineY][coordOrigineX].getPion()
        }else if (coordOrigineY>=4 && coordDestinationY<=3 ){
            pionArriveDeZone=plateau.getCases()[coordOrigineY][coordOrigineX].getPion()
            emplacementpionarrivedezone= Coordonnee(coordDestinationX,coordDestinationY)
            //changement de zone
        }else{
            pionArriveDeZone=null
            emplacementpionarrivedezone= null
        }
        //-----------------FIN CHANGEMENT DE ZONE
        //DEPLACEMENT PION

        setCoordOrigineDeplacement(coordOrigine!!)
        setCoordDestinationDeplacement(coordDest!!)

        plateau.getCases()[coordDestinationY][coordDestinationX].setPion(plateau.getCases()[coordOrigineY][coordOrigineX].getPion())

        plateau.getCases()[coordOrigineY][coordOrigineX].setPion(null)
        //--------------------Changement joueur courant

        changeJoueurCourant()
        if (joueur2 is IA && joueurCourant == joueur2){
            print("c'est à mon tour")
            deplacementia()

            }

    }

    override fun joueurVainqueur(): Joueur? {
        var a = arretPartie()

        if (arretPartie()){
            if (joueur1!!.calculerScore() > joueur2!!.calculerScore()) {
                return joueur1
            }
            else if (joueur1!!.calculerScore() == joueur2!!.calculerScore()) {
                return null
            }
            else {
                return joueur2
            }
        }

        if (joueurCourant!!.getNbPionsCaptures() == pionsCaptures.size){
            a = true
            joueurCourant
        }

        return null
    }

    /**
     * permet de savoir si la partie est finie ou non
     * @return true si la partie est finie, false sinon
     */
    fun arretPartie(): Boolean {
        if (nombreCoupsSansPrise==nombreCoupsSansPriseMax) return true
        if (joueur1!!.getPionsCaptures().size == 9 || joueur2!!.getPionsCaptures().size == 9){
            return true
        }
        if (!deplacementEncorePossible(joueurCourant!!) && nombreCoupsSansPriseMax<=nombreCoupsSansPrise){
            return true
        }
        //println("arret partie")
        return false
    }

    fun deplacementEncorePossible(joueur:Joueur):Boolean{
        for (colone in 0 until plateau.getTailleVerticale()){
            for (ligne in 0 until plateau.getTailleHorizontale()){
                if (plateau.getCases()[colone][ligne].getJoueur()== joueur){
                    if (!plateau.getCases()[colone][ligne].estLibre() && plateau.getCases()[colone][ligne].getPion() != null ){
                        var a = deplacementFaisable(ligne,colone)
                        if (a.isNotEmpty()) return true


                    }
                }
            }
        }
        return false
    }

    fun deplacementFaisable(coX:Int,coY:Int):List<Deplacement>{
        require(coX in 0 until plateau.getTailleHorizontale())
        require(coY in 0 until plateau.getTailleVerticale())
        val listedeplacement = mutableListOf<Deplacement>()
        val coOrigine = Coordonnee(coX,coY)
        val caseOrigine = plateau.getCases()[coY][coX]
        val pionOrigine = caseOrigine.getPion()
        val joueurOrigine = caseOrigine.getJoueur()
        if (caseOrigine.estLibre()) return listedeplacement.toList()
        for ((x,ligne) in plateau.getCases().withIndex()){
            for ((y,caseVerif) in ligne.withIndex()) {
                if (x == coX && y == coY) continue
                if (!caseVerif.estLibre() && joueurOrigine == caseVerif.getJoueur()) continue
                if (pionArriveDeZone == pionOrigine && joueurOrigine != caseVerif.getJoueur()) continue
                val codest = Coordonnee(x,y)
                val depl = Deplacement(coOrigine, codest)
                try {
                    val chemin = caseVerif.getPion()!!.getDeplacement(depl)
                    for (coord in chemin.subList(0, chemin.lastIndex)) {
                        if (!plateau.getCases()[coord.getY()][coord.getX()].estLibre()) {
                            throw DeplacementException()
                        }
                    }
                } catch (e: DeplacementException) {
                    continue
                }
                listedeplacement.add(depl)
            }
        }
        return listedeplacement.toList()
    }

    /**
     * modifie le joueur courant
     *
     */
    fun changeJoueurCourant() {
        if (joueurCourant == joueur1){
            joueurCourant = joueur2
        }
        else {
            joueurCourant = joueur1
        }
    }
    fun exportsave(nomsave:String="src/main/resources/partieSave/sauvegardeEchechmartien.json"){







        var text = JsonObject()
        var pj1G=0
        var pj1M=0
        var pj1P=0
        var pj2G=0
        var pj2M=0
        var pj2P=0
        var pionmj1 = ""
        var pionmj2 = ""
        for (i in joueur1!!.getPionsCaptures()){
            if (i is MoyenPion){
                pionmj1+="M"
            }else if(i is GrandPion){
                pionmj1+="G"
            }else if(i is PetitPion){
                pionmj1+="P"
            }
        }
        for (i in joueur2!!.getPionsCaptures()){
            if (i is MoyenPion){
                pionmj2+="M"
            }else if(i is GrandPion){
                pionmj2+="G"
            }else if(i is PetitPion){
                pionmj2+="P"
            }
        }
        text.addProperty("joueurs1",joueur1?.getPseudo())
        text.addProperty("joueurs2",joueur2?.getPseudo())
        text.addProperty("joueurcourant",joueurCourant?.getPseudo())
        text.addProperty("plateau",plateau.toString().replace("\n",""))
        text.addProperty("CopionarrivedezoneX",emplacementpionarrivedezone?.getX().toString())
        text.addProperty("CopionarrivedezoneY",emplacementpionarrivedezone?.getY().toString())
        text.addProperty("pionmangeeJ1",pionmj1)
        text.addProperty("pionmangeeJ2",pionmj2)
        text.addProperty("nbactuel",nombreCoupsSansPrise.toString())
        text.addProperty("nbactuelmax",nombreCoupsSansPriseMax.toString())
        var file = File(nomsave)
        if (!file.exists()){
            file.createNewFile()
        }else{
            var changenom= 1
            while(File(nomsave.replace(".","$changenom"+".").toString()).exists()){
                changenom+=1

            }
            print("iciiiiii")
            file = File(nomsave.replace(".","$changenom"+"."))

        }


        val writer = FileWriter(file)
        print(file)
        Gson().toJson(text, writer)
        writer.flush()
        writer.close()


    }
    fun importjson(namefile:String){
        print(namefile)
        var emplacement = namefile
        val reader = FileReader(emplacement)
        val lejeux = Gson().fromJson(reader,JsonObject::class.java)
        joueur1=Joueur(lejeux["joueurs1"].asString)

        joueur2= Joueur(lejeux["joueurs2"].asString)
        if (joueur1!!.getPseudo() == lejeux["joueurcourant"].asString ){
            joueurCourant=joueur1
        }else if(joueur2!!.getPseudo() == lejeux["joueurcourant"].asString ){
            joueurCourant=joueur2
        }
        var plateaustrings = lejeux["plateau"].asString
        var x= 0
        var y = 0


        var plateaubis = Plateau()
        for (i in plateaustrings){
            if (x==4){
                x=0
                y+=1
            }
            if (i.toString()==("G") ){
                plateaubis.getCases()[y][x].setPion(GrandPion())
                if (y<4){
                    plateaubis.getCases()[y][x].setJoueur(joueur1)
                }else{
                    plateaubis.getCases()[y][x].setJoueur(joueur2)
                }
                x+=1
            }else if (i.toString()==("M")){
                plateaubis.getCases()[y][x].setPion(MoyenPion())
                if (y<4){
                    plateaubis.getCases()[y][x].setJoueur(joueur1)
                }else{
                    plateaubis.getCases()[y][x].setJoueur(joueur2)
                }
                x+=1
            }else if (i.toString()==("P")){
                plateaubis.getCases()[y][x].setPion(PetitPion())
                if (y<4){
                    plateaubis.getCases()[y][x].setJoueur(joueur1)
                }else{
                    plateaubis.getCases()[y][x].setJoueur(joueur2)
                }
                x+=1
            }else{
                plateaubis.getCases()[y][x].setPion(null)
                if (y<4){
                    plateaubis.getCases()[y][x].setJoueur(joueur1)
                }else{
                    plateaubis.getCases()[y][x].setJoueur(joueur2)
                }
                x+=1
            }
        }

        plateau=plateaubis

        var pionpris1 = lejeux["pionmangeeJ1"].asString
        var pionpris2 = lejeux["pionmangeeJ2"].asString
        for (i in 0 until pionpris1.length){
            if (i.toString()=="G") joueur1!!.ajouterPionCaptures(GrandPion())
            else if (i.toString()=="M") joueur1!!.ajouterPionCaptures(MoyenPion())
            else if (i.toString()=="P") joueur1!!.ajouterPionCaptures(PetitPion())
        }
        for (i in 0 until pionpris2.length){
            if (i.toString()=="G") joueur2!!.ajouterPionCaptures(GrandPion())
            else if (i.toString()=="M") joueur2!!.ajouterPionCaptures(MoyenPion())
            else if (i.toString()=="P") joueur2!!.ajouterPionCaptures(PetitPion())
        }


        var pionarriveY : Int?
        var pionarriveX : Int?

        try{
            pionarriveY=lejeux["CopionarrivedezoneY"].asInt
            pionarriveX=lejeux["CopionarrivedezoneX"].asInt
        }catch (e : NumberFormatException){
            pionarriveY = null
            pionarriveX = null
        }

        if (pionarriveY != null && pionarriveX != null){
            pionArriveDeZone=plateau.getCases()[pionarriveY][pionarriveX].getPion()
        }else{
            pionArriveDeZone=null
        }


        nombreCoupsSansPrise = lejeux["nbactuel"].asInt
        nombreCoupsSansPriseMax = lejeux["nbactuelmax"].asInt

    }
    fun deplacementia(){
        if (joueur2 is IA && joueur2 != null){
            print("4444444444444444444444444")
            var  coup = (joueur2 as IA).allcouppossible()
            print("\n")
            print("je suis dans deplacementia")
            print(coup)
            print(coup.isEmpty())
            print("\n")
            deplacer(coup[0].getX(),coup[0].getY(),coup[1].getX(),coup[1].getY())
        }
    }
}