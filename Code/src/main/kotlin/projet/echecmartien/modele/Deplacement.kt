package projet.echecmartien.modele

import projet.echecmartien.exceptions.DeplacementException
import projet.echecmartien.librairie.GeneralData
import java.lang.IllegalArgumentException
import kotlin.math.absoluteValue


/**
 * cette classe permet de tester les déplacements sur le plateau de jeu
 *
 */

class Deplacement(origine : Coordonnee, destination : Coordonnee) {
    private var origine : Coordonnee
    private var destination : Coordonnee

    /**
     * dans le constructeur la validité du déplacement dans la grille est testée
     *@throws DeplacementException si le déplacement n'est ni horizontal, ni vertical est ni diagonal
     * les autres cas lèvent une IllegalArgumentException (peut être mis en place avec "require")
     */
    init{
        this.origine = origine
        this.destination = destination

        /*
        * Doit vérifier si dans un de ces 3 cas : origineX != de destinationX et origineY == destinationY ou  origineX == de destinationX et origineY != destinationY
        * ou destinationX-origineX == destinationY - origineY
        *
        */

        require(
            origine.getX() in 0..GeneralData().tableau.size-1 && destination.getX() in 0..GeneralData().tableau.size-1 &&
                    origine.getY() in 0..GeneralData().tableau[0].size-1 && destination.getY() in 0..GeneralData().tableau[0].size-1
        ){
            throw IllegalArgumentException()
        }

        require(
            estHorizontal() || estVertical() || estDiagonal()
        ){
            throw DeplacementException()
        }
    }


    /**
     * getter
     * @return la destination de ce déplacement
     */
    fun getDestination():Coordonnee{
        return destination
    }


    /**
     * getter
     * @return l'origine de ce déplacement
     */
    fun getOrigine():Coordonnee{
       return origine
    }

    /**
     *méthode qui permet de tester si le déplacement est horizontal
     * @return true si le déplacement est horizontal, false sinon
     */
    fun estHorizontal() : Boolean {
        if (origine.getY() == destination.getY() ){
            return true
        }
        return false
    }

    /**
     *méthode qui permet de tester si le déplacement est vertical
     * @return true si le déplacement est vertical, false sinon
     */
    fun estVertical(): Boolean {
        if (origine.getX() == destination.getX() ){
            return true
        }
        return false

    }

    /**
     * méthode qui permet de tester si le déplacement est diagonal
     * @return true si le déplacement est diagonal, false sinon
     */
    fun estDiagonal():Boolean {
        val a = destination.getX() - origine.getX()
        val b = destination.getY() - origine.getY()

        if (a.absoluteValue == b.absoluteValue) {
            return true
        }
        return false

    }

    /**
     *méthode qui permet de calculer le nombre de case d'un déplacement
     * @return le nombre de case que le pion sera déplacée
     */
    fun longueur(): Int {
        if (estHorizontal()){
            return (destination.getX()-origine.getX()).absoluteValue
        }
        if (estVertical()){
            return (destination.getY()-origine.getY()).absoluteValue

        }
        if (estDiagonal()){
            return (destination.getX()-origine.getX()).absoluteValue
        }
        return -1

    }


    /**
     * méthode qui permet de déterminer le sens d'un déplacement vertical
     *
     *@return true si le déplacement est positif, false sinon
     */
    fun estVerticalPositif(): Boolean{
        return ((destination.getY() - origine.getY()) > 0 && estVertical())
    }

    /**
     * méthode qui permet de déterminer le sens d'un déplacement horizontal
     *
     * @return true si le déplacement est positif, false sinon
     */
    fun estHorizontalPositif():Boolean{
        return ((destination.getX() - origine.getX()) > 0 && estHorizontal())
    }

    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est positif en X et en Y
     *
     * @return true si le déplacement est positif en X et Y, false sinon
     */
    fun estDiagonalPositifXPositifY() : Boolean{
       return ((destination.getY() - origine.getY()) > 0 && (destination.getX() - origine.getX()) > 0 && estDiagonal())
    }
    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est négatif en X et positif en Y
     *
     * @return true si le déplacement est négatif en X et positif en Y, false sinon
     */
    fun estDiagonalNegatifXPositifY(): Boolean{
        return ((destination.getY() - origine.getY()) > 0 && (destination.getX() - origine.getX()) < 0 && estDiagonal())
    }

    /**
     *
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est positif en X et négatif en Y
     *
     * @return true si le déplacement est positif en X et négatif en Y, false sinon
     */
    fun estDiagonalPositifXNegatifY(): Boolean{
        return ((destination.getY() - origine.getY()) < 0 && (destination.getX() - origine.getX()) > 0 && estDiagonal())

    }

    /**
     * méthode qui permet de déterminer si le sens d'un déplacement diagonal est négatif en X et négatif en Y
     *
     * @return true si le déplacement est négatif en X et négatif en Y, false sinon
     */
    fun estDiagonalNegatifXNegatifY(): Boolean{
        return ((destination.getY() - origine.getY()) < 0 && (destination.getX() - origine.getX()) < 0 && estDiagonal())

    }

    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplacement demandé est vertical.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée
     * si le déplacement est vertical. Le point de départ n'est pas stocké dans la liste.
     * @throws DeplacementException est levée si le déplacement n'est pas vertical
     */
    fun getCheminVertical(): List<Coordonnee> {
        var liste = mutableListOf<Coordonnee>()
        var longueur = longueur()
        if (! estVertical()){
            throw DeplacementException()
        }
        if (estVerticalPositif()){
            for (ligne in 1 until longueur+1){
                liste.add( Coordonnee(origine.getX(),origine.getY()+ligne))
            }
        }else{
            for (ligne in 1 until longueur+1){
            liste.add( Coordonnee(origine.getX(),origine.getY()-ligne))
            }
        }
        return liste.toList()
    }


    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplaceme{"origine Y dépasse"}nt demandé est horizontal.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée.
     * Le point de départ n'est pas stocké dans la liste.
     * si le déplacement est horizontal
     * @throws DeplacementException est levée si le déplacement n'est pas horizontal
     */
    fun getCheminHorizontal(): List<Coordonnee> {
        var liste = mutableListOf<Coordonnee>()
        var longueur = longueur()
        if (! estHorizontal()){
           throw DeplacementException()
       }
        if (estHorizontalPositif()){
            for (ligne in 1 until longueur+1){
                liste.add( Coordonnee(origine.getX()+ligne,origine.getY()))
            }
        }else{
            for (ligne in 1 until longueur+1){
                liste.add(Coordonnee(origine.getX()-ligne,origine.getY()))
            }
        }

       return liste.toList()

    }


    /**
     * donne le chemin de coordonnées que constitue le déplacement
     * du point de départ vers le point d'arrivée si le déplacement demandé est diagonal.
     * Le point de départ n'est pas stocké dans la liste.
     *
     * @return une liste de coordonnées qui constitue le déplacement du point de départ vers le point d'arrivée
     * si le déplacement est diagonal
     * @throws DeplacementException est levée si le déplacement n'est pas diagonal
     */
    fun getCheminDiagonal(): List<Coordonnee> {
        var liste = mutableListOf<Coordonnee>()
        var longueur = longueur()
        if (! estDiagonal()) {
            throw DeplacementException()
        }

        if (estDiagonalNegatifXPositifY()){

            for (ligne in 1 until longueur+1 ) {
                liste.add( Coordonnee(origine.getX() - ligne, origine.getY() + ligne))
            }

        }else if (estDiagonalNegatifXNegatifY()){
            for (ligne in 1 until longueur+1 ){
                liste.add(Coordonnee(origine.getX()-ligne,origine.getY()-ligne))
            }
        }else if (estDiagonalPositifXNegatifY()){
            for (ligne in 1 until longueur+1 ){
                liste.add(Coordonnee(origine.getX()+ligne,origine.getY()-ligne))
            }

        }else if (estDiagonalPositifXPositifY()){
            for (ligne in 1 until longueur+1 ){
                liste.add(Coordonnee(origine.getX()+ligne,origine.getY()+ligne))
            }
        }
        return liste.toList()

    }


}

