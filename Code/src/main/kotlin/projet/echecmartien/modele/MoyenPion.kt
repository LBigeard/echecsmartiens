package projet.echecmartien.modele

import projet.echecmartien.exceptions.DeplacementException


class MoyenPion : GrandPion(){


    override fun getScore(): Int {
        return 2
    }
    //Les pions moyens se déplacent verticalement, horizontalement et diagonalement de 1 ou 2 cases.

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        if (deplacement.longueur() > 2 || deplacement.getOrigine().getX() == deplacement.getDestination().getX() && deplacement.getOrigine().getY() == deplacement.getDestination().getY()) {
            throw DeplacementException()
        }
        if (deplacement.estDiagonal()){
            return deplacement.getCheminDiagonal()
        }else if(deplacement.estHorizontal()){
            return deplacement.getCheminHorizontal()
        }else {
            return deplacement.getCheminVertical()
        }

    }
}