package projet.echecmartien.modele

open class Joueur (pseudo:String){
    private var pseudo:String

    private var pionsCapturer:MutableList<Pion>
    init {
        this.pseudo=pseudo
        pionsCapturer= mutableListOf()
    }

    /**
     * récupére la liste des pions capturés
     * @return la liste des pions capturés qui est vide si aucun pion n'a été capturé
     */
    fun getPionsCaptures(): Set<Pion> {
      return pionsCapturer.toSet()
    }

    /**
     * ajout à la liste d'un pion qui a été capturé
     * @param pion à ajouter
     */
    fun ajouterPionCaptures(pion: Pion) {
        pionsCapturer.add(pion)
    }


    /**
     * permet de connaître le nombre de pions capturés
     * @return le nombre de pions capturés
     */
    fun getNbPionsCaptures(): Int {
       return pionsCapturer.size
    }


    /**
     * récupère la valeur du pseudo
     *
     * @return la valeur du pseudo
     */
    fun getPseudo(): String {
        return pseudo
    }



    /**
     * calcule le score du joueur
     * @return le score du joueur
     */
    fun calculerScore(): Int {
        var score = 0
        for (pion in pionsCapturer){
           score+= pion.getScore()
        }
        return score
    }

}
