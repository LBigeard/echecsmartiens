package projet.echecmartien.modele

import projet.echecmartien.exceptions.DeplacementException


open class GrandPion : Pion(){


    override fun getScore(): Int {
        return 3
    }

    //Les grands pions se déplacent verticalement, horizontalement et diagonalement de n cases (comme la dame aux Eches traditionnel).

    override fun getDeplacement(deplacement: Deplacement): List<Coordonnee> {
        if (deplacement.estDiagonal()){
          return deplacement.getCheminDiagonal()
        }else if(deplacement.estHorizontal()){
            return deplacement.getCheminHorizontal()
        }else {
            return deplacement.getCheminVertical()
        }
    }


}