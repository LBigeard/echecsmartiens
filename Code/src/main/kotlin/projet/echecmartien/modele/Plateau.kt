package projet.echecmartien.modele

import projet.echecmartien.librairie.EnumPion
import projet.echecmartien.librairie.GeneralData


class Plateau (){
    var data = GeneralData()
    private var tailleHorizontale:Int = data.getTailleh()
    private var tailleVerticale:Int = data.getTailleV()
    private var tabcase = Array(tailleVerticale){Array(tailleHorizontale,{Case()})}


    /**
     * initialise le plateau de jeu avec les pions
     */
    fun initialiser() {

        for (infoH in 0 until data.tableau.size) {
            for (infoV in 0 until data.tableau[infoH].size)
                if (data.tableau[infoH][infoV].name == "GRANDPION") {
                    tabcase[infoV][infoH].setPion(GrandPion())
                } else if (data.tableau[infoH][infoV].name == "MOYENPION") {
                    tabcase[infoV][infoH].setPion(MoyenPion())
                } else if (data.tableau[infoH][infoV].name == "PETITPION") {

                    tabcase[infoV][infoH].setPion(PetitPion())
                } else if (data.tableau[infoH][infoV].name == "LIBRE") {
                    tabcase[infoV][infoH].setPion(null)
                }
        }
    }


    override fun toString(): String {
        var string = ""
        for (ligne in this.tabcase) {
            for (case in ligne) {
                string += when (case.getPion()) {
                    is PetitPion -> "P"
                    is MoyenPion -> "M"
                    is GrandPion -> "G"
                    else -> "."
                }
            }
            string += "\n"
        }
        return string
    }

    /**
     * donne la taille horizontale du plateau
     * @return la taille horizontale du plateau
     */
    fun getTailleHorizontale(): Int {
        return tailleHorizontale
    }


    /**
     * donne la taille verticale du plateau
     * @return la taille verticale du plateau
     */
    fun getTailleVerticale(): Int {
       return tailleVerticale
    }


    /**
     * donne le tableau des cases du plateau
     * @return les cases du plateau
     */
    fun getCases(): Array<Array<Case>> {
        return tabcase
    }


}