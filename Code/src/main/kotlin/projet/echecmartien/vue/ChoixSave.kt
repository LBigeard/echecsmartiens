package projet.echecmartien.vue


import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.*
import projet.echecmartien.modele.getNameofSave

open class ChoixSave : BorderPane(){  //


    var liste : VBox
    var button : Button
    var tiltedlist  : TitledPane
    var gridPane : GridPane
    var vboxPadding : VBox
    var selected :Int
    var buttonValider : Button
    var gridPading :GridPane
    var vboxPadding2 : VBox


    init{
        this.buttonValider = Button("VALIDER")
        this.buttonValider.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        selected = -1
        this.vboxPadding2 = VBox()
        this.gridPading = GridPane()
        this.gridPane = GridPane()
        this.gridPane.vgap = 15.0
        this.liste = VBox() // remplaçable par grid pane si besoin

        this.button = Button("Retour a la page d'acceuil")
        this.button.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")


        //this.liste.children.addAll(Label("test0"),Label("test"))
        /*for (i in 0 until listesave.size){
            this.liste.children.add(Label(listesave[i].replace("src/main/resources/partieSave/","").replace(".json","")))
        }
    */
        this.tiltedlist = TitledPane()

        /*for (i in 0 until listesave.size){
            this.gridPane.add(Label(listesave[i].replace("src/main/resources/partieSave/","").replace(".json","")),0,i)
        }*/


        this.vboxPadding = VBox()






        tiltedlist.isCollapsible = false

        tiltedlist.text = "Choix Sauvegarde"








        tiltedlist.content = gridPane

        this.children.add(buttonValider)
        vboxPadding.children.add(button)
        /////////////////TODO()
        this.center = tiltedlist
        this.bottom = this.gridPading
        this.vboxPadding2.alignment = Pos.CENTER_LEFT
        this.vboxPadding.alignment = Pos.CENTER
        this.vboxPadding2.children.add(buttonValider)

        this.gridPading.add(this.vboxPadding2,0,0)
        this.gridPading.add(this.vboxPadding,1,0)
        this.gridPading.alignment = Pos.CENTER
        this.gridPading.hgap = 20.0


        // Visuel :


        tiltedlist.maxHeight = Double.MAX_VALUE
        tiltedlist.maxWidth = Double.MAX_VALUE

        tiltedlist.padding = Insets(40.0,40.0,40.0,40.0)



        tiltedlist.alignment = Pos.CENTER
        vboxPadding.padding = Insets(0.0,15.0,15.0,0.0)

        vboxPadding.alignment = Pos.BASELINE_RIGHT










    }

    fun style (){
        this.buttonValider.styleClass.add("hoverAccueil")
        this.button.styleClass.add("hoverAccueil")
        this.styleClass.add("choixsave")
    }



    fun update(listesave : List<String> ){
        this.gridPane.children.clear()

        for (i in 0 until listesave.size){
            this.gridPane.add(Label(listesave[i].replace("src/main/resources/partieSave/","").replace(".json","")),0,i)
        }
    }


}