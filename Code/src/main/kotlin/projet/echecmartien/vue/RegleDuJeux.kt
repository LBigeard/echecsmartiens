package projet.echecmartien.vue

import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.geometry.VPos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.*
import javafx.scene.paint.Color
import java.io.FileInputStream

open class Reglesdujeux: HBox(){
    var regle : Label
    var titre : Label
    var gridPane : GridPane
    var vbox : VBox
    var img : Image
    var retour : Button
    var vboxdroite : VBox
    init{
        this.vboxdroite= VBox()
        this.retour = Button("Retour")
        this.img= Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/srcImage/exempleplateau.png"))
        this.gridPane = GridPane()
        this.vbox = VBox()
        this.titre = Label("Regles du jeu :")
        this.regle = Label("PREPARATION\n" +
                "Disposez les 18 pions comme sur la figure ci-contre.\n" +
                "Un joueur identifie ses pièces par leur position à un instant donné.\n" +
                "Le damier est divisé en 2 zones, une pour chaque joueur.\n"
                +"Toute pièce dans la zone d'un joueur est la sienne.\n" +
                "\n" +
                "DEROULEMENT DU JEU\n" +
                "Chaque joueur, à son tour de jeu, déplace une de ses pièces.\n" +
                "Les grands pions se déplacent verticalement, horizontalement et \n"+
                "diagonalement de n cases (comme la dame aux Eches traditionnel).\n" +
                "Les pions moyens se déplacent verticalement, horizontalement et\n" +
                "diagonalement de 1 ou 2 cases.\n" +
                "Les petits pions se déplacent diagonalement de 1 case.\n" +
                "A son tour de jeu un joueur peut déplacer n'importe quel pion \n"+
                "de son camp, soit à l'intérieur de sa zone soit vers la zone adverse.\n"+
                "\n" +
                "Exception: Il est interdit de renvoyer dans la zone adverse un\n"+
                "pion qui vient d'arriver dans sa zone. Mais on peut déplacer ce\n" +
                "même pion à l'intérieur de sa zone\n" +
                "\n" +
                "On capture un pion adverse en prenant sa place (donc fatalement en \n"+
                "prenant un pion de sa zone et en allant dans la zone adverse). Le \n"+
                "pion capturé est retiré du damier..\n" +
                "Le saut par dessus un ou n pions adverses ou non n'est pas autorisé.\n" +
                "\n" +
                "FIN DE LA PARTIE\n" +
                "Une fois la partie finie (plus de pions à capturer car ils sont tous\n"+
                "capturés ou plus aucunes prises n'est possibles), on compte\n" +
                "3 points par grand pion capturés, 2 par moyen et 1 par petit.\n" +
                "\n" +
                "Le gagant est évidement le joueur qui à le plus de points.")



        gridPane.add(titre,1,0)
        titre.padding= Insets(0.0,0.0,20.0,150.0)
        titre.style= "-fx-text-fill: #FFFFFF;" +
                "-fx-font-weight: bold;\n" +
                "    -fx-font-size: 20;\n"
        gridPane.add(regle,1,1)
        regle.style= "-fx-text-fill: #ffffff;"




        gridPane.isGridLinesVisible=false
        gridPane.padding= Insets(0.0,40.0,40.0,70.0)

        vboxdroite.children.addAll(ImageView(img), retour)
        vboxdroite.alignment=Pos.BOTTOM_CENTER
        vboxdroite.spacing=300.0
        vboxdroite.padding=Insets(0.0,0.0,0.0,100.0)

        vbox.alignment=Pos.CENTER
        vbox.children.addAll(gridPane)
        this.children.add(vbox)
        this.children.add(vboxdroite)
        this.padding=Insets(20.0)
        retour.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        retour.textFill=Color.BLACK



    }

    fun style (){
        this.retour.styleClass.add("hoverAccueil")
        this.styleClass.add("jeuxbg")
    }



}