package projet.echecmartien.vue

import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import projet.echecmartien.AppliJeuEchecMartien


open class Jeux : BorderPane() {
    var gauche : BorderPane
    var droite : BorderPane
    var centre : GridPane
    var infojoueur2 : GridPane
    var labelInfoJoueur2 : Label
    var labelNombrePionspris : Label
    var labelTypeDePions : Label

    var infojoueur1 : GridPane
    var labelInfoJoueur1 : Label
    var labelNombrePionspris1 : Label
    var labelTypeDePions1 : Label


    var rows : Int
    var columns : Int


    var labelNombrePetitJ1 : Label
    var labelNombrePetitJ2 : Label

    var labelNombreMoyenJ1 : Label
    var labelNombreMoyenJ2 : Label

    var labelNombreGrandJ1 : Label
    var labelNombreGrandJ2 : Label

    var petitCercle : Circle
    var moyenCercle : Circle
    var grandCercle : Circle

    var petitCercle1 : Circle
    var moyenCercle1 : Circle
    var grandCercle1 : Circle

    var buttonQuit : Button
    var buttonRegle : Button
    var gridButton : GridPane
    var chexboxsave : CheckBox




    var labelJ2 : Label
    var labelJ1 : Label

    init {
        this.infojoueur1 = GridPane()
        this.labelInfoJoueur1 = Label("Informations joueur 2")
        this.labelNombrePionspris1 = Label("Nombre")
        this.labelTypeDePions1 = Label("Type de pions")

        this.buttonQuit = Button("QUITTER")
        this.buttonQuit.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        this.buttonRegle = Button("AFFICHER LES REGLES")
        this.buttonRegle.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")

        this.gridButton = GridPane()
        this.chexboxsave = CheckBox("Sauvegarder ?")





        this.labelJ2 = Label("Pseudo Joueur 2")
        this.labelJ1 = Label("Pseudo Joueur 1")


        this.petitCercle = Circle(12.0)
        this.moyenCercle = Circle(17.0)
        this.grandCercle = Circle(23.0)
        this.petitCercle1 = Circle(12.0)
        this.moyenCercle1 = Circle(17.0)
        this.grandCercle1 = Circle(23.0)

        this.labelNombrePetitJ1 = Label("0")
        this.labelNombrePetitJ2 = Label("0")

        this.labelNombreMoyenJ1 = Label("0")
        this.labelNombreMoyenJ2 = Label("0")

        this.labelNombreGrandJ1 = Label("0")
        this.labelNombreGrandJ2 = Label("0")


        this.infojoueur2 = GridPane()
        this.labelInfoJoueur2 = Label("Informations joueur 1")
        this.labelNombrePionspris = Label("Nombre")
        this.labelTypeDePions = Label("Type de pions")


        this.gauche = BorderPane()
        this.droite = BorderPane()
        this.centre = GridPane()
        this.rows = 8
        this.columns = 4



        //centre.styleClass.add("game-grid")

        /*for (i in 0 until columns) {
            val column = ColumnConstraints(77.0)
            centre.columnConstraints.add(column)
        }

        for (i in 0 until rows) {
            val row = RowConstraints(77.0)
            centre.rowConstraints.add(row)
        }*/





        for (i in 0 until columns) {//row
            for (j in 0 until rows) {  //collonne
                var newpane = StackPane() //Pane()
                newpane.setPrefSize(77.0,77.0)
                //newpane.children.add(Circle(newpane.width/2,newpane.height/2,25.0))// utilisé binding comme td6?

                //var point = Point2D(50.0,20.0) Rectangle(60.0,60.0)
                //newpane.contains(point)
                if (j in 0 .. 3 ){
                    if (j % 2 == 0) {
                        if (i % 2 == 0) {
                            //newpane.fill = Color.RED
                            newpane.style = "-fx-background-color:   #b34253    ;"+"-fx-opacity: 0.8"
                        }else{
                            newpane.style = "-fx-background-color:   #c9c3c9     ;" + "-fx-opacity: 0.8"
                        }
                    }else{
                        if (i % 2 != 0) {
                            //newpane.fill = Color.RED
                            newpane.style = "-fx-background-color:    #b34253    ;"+"-fx-opacity: 0.8"
                        }else{
                            newpane.style = "-fx-background-color:     #c9c3c9   ;"+"-fx-opacity: 0.8"
                        }
                    }
                }else{
                    if (j % 2 == 0) {
                        if (i % 2 == 0) {
                            //newpane.fill = Color.BLUE
                            newpane.style = "-fx-background-color:     #5766c9    ;"+"-fx-opacity: 0.8"
                        }else{
                            newpane.style = "-fx-background-color:     #c9c3c9    ;"+"-fx-opacity: 0.8"
                        }
                    }else{
                        if (i % 2 != 0) {
                            //newpane.fill = Color.BLUE
                            newpane.style = "-fx-background-color:    #5766c9     ;"+"-fx-opacity: 0.8"
                        }else{
                            newpane.style = "-fx-background-color:     #c9c3c9     ;"+"-fx-opacity: 0.8"
                        }
                    }
                }

                /*newpane.styleClass.add("game-grid-cell");
                if (i == 0) {
                    newpane.styleClass.add("first-column");
                }
                if (j == 0) {
                    newpane.styleClass.add("first-row");
                }*/



                //point.setStroke()

                centre.add(newpane,i,j)


            }
        }
        // amodiff voir adaptation taille




        centre.alignment = Pos.CENTER
        //centre.isGridLinesVisible = true  // a modiff car que pour debug? plussieur solution : couleur differnete et en quinquonce en haut et en bas, ligne visible par autre moyen, couleur exterieur rectangle differnete de interieur

        //this.center = canvas
        //var mutabletest = mutableListOf<Node>(labelInfoJoueur2,labelInfoJoueur2,labelNombrePionspris,labelTypeDePions,labelNombrePetitJ1,petitCercle,labelNombreMoyenJ1,moyenCercle,labelNombreGrandJ1,grandCercle)
        // mutabletest.addAll(labelInfoJoueur2,labelNombrePionspris,labelTypeDePions,labelNombrePetitJ1,petitCercle,labelNombreMoyenJ1,moyenCercle,labelNombreGrandJ1,grandCercle)

        infojoueur2.styleClass.add("game-grid")
        infojoueur1.styleClass.add("game-grid")




        //var count  = 0
        for (i in 0 until 2) {
            for (j in 0 until 5) {
                var newpaneinfo = Pane() //Pane()

                //newpaneinfo.children.add(mutabletest[count])
                //count += 1
                newpaneinfo.setPrefSize(60.0, 60.0)
                //newpaneinfo.style = "-fx-background-color: Grey ;"
                newpaneinfo.styleClass.add("game-grid-cell")
                if (i == 0) {
                    newpaneinfo.styleClass.add("first-column")
                }
                if (j == 0) {
                    newpaneinfo.styleClass.add("first-row")
                }
                infojoueur2.add(newpaneinfo, i, j)

                var newpaneinfo1 = Pane() //Pane()

                //newpaneinfo.children.add(mutabletest[count])
                //count += 1
                newpaneinfo1.setPrefSize(60.0, 60.0)
                //newpaneinfo.style = "-fx-background-color: Grey ;"
                newpaneinfo1.styleClass.add("game-grid-cell-blue")
                if (i == 0) {
                    newpaneinfo1.styleClass.add("first-column")
                }
                if (j == 0) {
                    newpaneinfo1.styleClass.add("first-row")
                }
                infojoueur1.add(newpaneinfo1, i, j)
            }
        }

        infojoueur2.add(labelInfoJoueur2,0,0,2,1)
        infojoueur2.add(labelNombrePionspris,0,1,1,1)
        infojoueur2.add(labelTypeDePions,1,1,1,1)
        infojoueur2.add(labelNombrePetitJ2,0,2,1,1)
        infojoueur2.add(petitCercle,1,2,1,1)
        infojoueur2.add(labelNombreMoyenJ2,0,3,1,1)
        infojoueur2.add(moyenCercle,1,3,1,1)
        infojoueur2.add(labelNombreGrandJ2,0,4,1,1)
        infojoueur2.add(grandCercle,1,4,1,1)

        GridPane.setHalignment(labelInfoJoueur2, HPos.CENTER)
        GridPane.setHalignment(labelNombrePionspris, HPos.CENTER)
        GridPane.setHalignment(labelTypeDePions, HPos.CENTER)
        GridPane.setHalignment(labelNombrePetitJ2, HPos.CENTER)
        GridPane.setHalignment(petitCercle, HPos.CENTER)
        GridPane.setHalignment(labelNombreMoyenJ2, HPos.CENTER)
        GridPane.setHalignment(moyenCercle, HPos.CENTER)
        GridPane.setHalignment(labelNombreGrandJ2, HPos.CENTER)
        GridPane.setHalignment(grandCercle, HPos.CENTER)

        infojoueur1.add(labelInfoJoueur1,0,0,2,1)
        infojoueur1.add(labelNombrePionspris1,0,1,1,1)
        infojoueur1.add(labelTypeDePions1,1,1,1,1)
        infojoueur1.add(labelNombrePetitJ1,0,2,1,1)
        infojoueur1.add(petitCercle1,1,2,1,1)
        infojoueur1.add(labelNombreMoyenJ1,0,3,1,1)
        infojoueur1.add(moyenCercle1,1,3,1,1)
        infojoueur1.add(labelNombreGrandJ1,0,4,1,1)
        infojoueur1.add(grandCercle1,1,4,1,1)

        GridPane.setHalignment(labelInfoJoueur1, HPos.CENTER)
        GridPane.setHalignment(labelNombrePionspris1, HPos.CENTER)
        GridPane.setHalignment(labelTypeDePions1, HPos.CENTER)
        GridPane.setHalignment(labelNombrePetitJ1, HPos.CENTER)
        GridPane.setHalignment(petitCercle1, HPos.CENTER)
        GridPane.setHalignment(labelNombreMoyenJ1, HPos.CENTER)
        GridPane.setHalignment(moyenCercle1, HPos.CENTER)
        GridPane.setHalignment(labelNombreGrandJ1, HPos.CENTER)
        GridPane.setHalignment(grandCercle1, HPos.CENTER)






        //infojoueur2.alignment = Pos.CENTER


    //infojoueur2.style = "-fx-background-color : BLUE;"
    //infojoueur2.isGridLinesVisible = true

        gridButton.add(buttonQuit,0,0,1,1)
        gridButton.add(buttonRegle,1,0,1,1)
        gridButton.add(chexboxsave,0,1,1,1)




        chexboxsave.isSelected = true



        gauche.top = infojoueur2
        gauche.bottom = gridButton

        gauche.center = labelJ2


        //infojoueur2.alignment = Pos.TOP_RIGHT



        droite.bottom = infojoueur1
        droite.center = labelJ1

        //labelJ1.alignment = Pos.TOP_LEFT


        //infojoueur2.alignment = Pos.TOP_RIGHT


        this.right = droite
        this.left = gauche
        this.center = centre // a gauche car rien a droite pour l'instant


        //labelJ2.padding = Insets(0.0,0.0,100.0,200.0)
        gridButton.padding = Insets(0.0,0.0,10.0,10.0)
        infojoueur2.padding = Insets(10.0,0.0,0.0,10.0)
        infojoueur1.padding = Insets(0.0,10.0,10.0,115.0)
        gridButton.vgap = 5.0
        chexboxsave.style ="-fx-text-fill : white ;"+"-fx-font-weight : BOLD; "

        labelJ1.style = "-fx-text-fill : #b34253 ;"+"-fx-font-size : 20.0; "+ "-fx-font-weight : BOLD;"
        labelJ2.style = "-fx-text-fill : #5766c9 ;"+"-fx-font-size : 20.0; "+ "-fx-font-weight : BOLD; "
        //labelJ2.setMaxSize(130.0,0.0)
        //labelJ2.isDisable = true
        BorderPane.setMargin(labelJ2, Insets(0.0, 0.0, 0.0, 100.0)) // optional
        BorderPane.setMargin(labelJ1, Insets(0.0, 100.0, 0.0, 0.0)) // optional



        this.styleClass.addAll("jeuxbg")





    }


    fun eventAddCase(eventHandler: EventHandler<MouseEvent>) {
        //this.updatepion?(livres,index)

        this.centre.children.forEach { it.onMouseClicked = eventHandler }

    }

    fun style (){
        this.chexboxsave.styleClass.add("radiobox")
        this.buttonQuit.styleClass.add("hoverAccueil")
        this.buttonRegle.styleClass.add("hoverAccueil")

    //scene.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
    //this.styleClass.addAll("jeuxbg")
    //this.infojoueur2.styleClass.add("jeuxbginfojoueur2")
    //scene.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
    }
    fun getPane(col: Int, row: Int): Pane {
        for (element in this.centre.children) {
            if (GridPane.getColumnIndex(element) == col && GridPane.getRowIndex(element) == row) {
                return element as Pane
            }
        }
        throw(IllegalArgumentException())
    }


}