package projet.echecmartien.vue

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import javafx.scene.text.Text


class Choixparametres: VBox() {

    var nbjoueur1 = RadioButton("1 joueur")
    var nbjoueur2 = RadioButton("2 joueurs")
    var buttonvalidee = Button("VALIDER")
    var textfildPseudo1 = TextField()
    var textfildPseudo2 = TextField()
    var textfildNbcoupmax= TextField()
    var grid = GridPane()
    var titre = Text("PARAMETRES DE LA PARTIE")
    val choixnbjoueurs = ToggleGroup()
    var optionsup = Text("Nombre de coups maximum sans prises :")
    var chxnbjoueur = Text("Choix du nombre de joueur:")
    var chxpseudo =Text("Choix des pseudos:")
    var allignementgridH = HBox()
    var allignementgridV = VBox()
    var Hboxdubuttonquitter = HBox()
    var buttonquitter = Button("Quitter")
    var vboxdubuttonquitter = VBox()
    var buttoninvisible1 = Button("je suis invisible")
    var buttoninvisible2 = Button("je suis invisible")

    init{
        this.children.add(buttoninvisible1)
        this.children.add(titre)
        this.buttoninvisible1.padding = Insets(0.0,0.0,150.0,0.0)
        this.allignementgridV.children.add(grid)
        this.allignementgridH.children.add(allignementgridV)
        this.children.add(allignementgridH)
        allignementgridH.alignment = Pos.CENTER
        this.allignementgridV.alignment = Pos.CENTER
        this.nbjoueur2.isSelected=true
        textfildPseudo1.setMinSize(150.0,25.0)
        textfildPseudo1.setMaxSize(150.0,25.0)

        textfildPseudo1.setPromptText("Pseudo Joueur 1")
        textfildPseudo2.setPromptText("Pseudo Joueur 2")


        textfildPseudo2.setMinSize(150.0,25.0)
        textfildPseudo2.setMaxSize(150.0,25.0)
        this.alignment = Pos.CENTER


        grid.add(chxnbjoueur,0,1)

        grid.add(nbjoueur1,0,2)
        grid.add(nbjoueur2,1,2)

        nbjoueur1.setToggleGroup(choixnbjoueurs)
        nbjoueur2.setToggleGroup(choixnbjoueurs)

        grid.add(chxpseudo,0,3)

        grid.add(textfildPseudo1,0,4)
        grid.add(textfildPseudo2,1,4)

        grid.add(optionsup,0,5)
        grid.add(textfildNbcoupmax,0,6)
        this.children.add(buttonvalidee)
        //Ajout de css

        this.nbjoueur1.setStyle("-fx-text-fill: silver;")
        this.nbjoueur2.setStyle("-fx-text-fill: silver ;")


        textfildNbcoupmax.setMinSize(50.0,25.0)
        textfildNbcoupmax.setMaxSize(50.0,25.0)

        //this.textfilNbcoupmax.alignment = Pos.CENTER

        this.titre.setFill(Color.BEIGE);
        this.chxpseudo.setFill(Color.BEIGE)
        this.chxnbjoueur.setFill(Color.BEIGE)
        this.optionsup.setFill(Color.BEIGE)


        this.buttoninvisible2.padding = Insets(0.0,0.0,100.0,0.0)
        this.buttoninvisible1.isVisible = false
        this.buttoninvisible2.isVisible = false
        this.buttoninvisible1.isDisable = true
        this.buttoninvisible2.isDisable = true
        this.vboxdubuttonquitter.children.add(buttoninvisible2)
        HBox.setMargin(buttonquitter, Insets(0.0,0.0,50.0,50.0))
        this.grid.vgap = 10.0
        this.grid.hgap = 15.0

        this.buttonvalidee.padding = Insets(5.0,20.0,5.0,20.0)
        buttonvalidee.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        this.children.add(vboxdubuttonquitter)
        this.vboxdubuttonquitter.children.add(this.Hboxdubuttonquitter)

        this.Hboxdubuttonquitter.padding = Insets(50.0,0.0,0.0,0.0)
        this.Hboxdubuttonquitter.children.add(buttonquitter)

        this.Hboxdubuttonquitter.alignment = Pos.BASELINE_LEFT

        this.buttonquitter.setMinSize(80.0,30.0)
        this.buttonquitter.setMaxSize(80.0,30.0)
        buttonquitter.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")

    }


    fun style(){
        this.titre.styleClass.add("pourlestitres")
        this.styleClass.add("jeuxbg")
        this.chxnbjoueur.styleClass.add("soustitres")
        this.chxpseudo.styleClass.add("soustitres")
        this.optionsup.styleClass.add("soustitres")
        this.buttonquitter.styleClass.add("hoverAccueil")
        this.buttonvalidee.styleClass.add("hoverAccueil")
        this.textfildPseudo1.styleClass.add("textfieldBack")
        this.textfildPseudo2.styleClass.add("textfieldBack")
        this.textfildNbcoupmax.styleClass.add("textfieldBack")
        this.nbjoueur1.styleClass.add("radiobox")
        this.nbjoueur2.styleClass.add("radiobox")

    }

    fun getChoix() : String{
        return textfildNbcoupmax.text
    }
}