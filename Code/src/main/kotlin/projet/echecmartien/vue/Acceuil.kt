package projet.echecmartien.vue

import javafx.geometry.HPos
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.layout.*
import javafx.scene.paint.Color


class Acceuil:HBox() {
    var butonjouer : Button
    var butonregles : Button
    var butonchargerunepartie :Button

    init{

        //val img = Image("https://cdn.discordapp.com/attachments/978592135429361714/984094667626905660/bg.png")

        //val img = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/srcImage/bg.png"))
        //val logo = Image(FileInputStream("src/main/kotlin/projet/echecmartien/vue/srcImage/logo.gif"))
        //this.style="-fx-background-color: #0f2026; "

        /*
        val bImg = BackgroundImage(
            logo,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.DEFAULT,
            BackgroundSize.DEFAULT
        )
        val bGround = Background(bImg)
        */



        //this.background = bGround
        this.butonjouer = Button("Jouer")
        butonjouer.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        butonjouer.textFill=Color.BLACK

        this.butonregles = Button("Afficher les règles")
        butonregles.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        butonregles.textFill=Color.BLACK
        this.butonchargerunepartie = Button("Charger une partie")
        butonchargerunepartie.style=("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        butonchargerunepartie.textFill=Color.BLACK
        var test = GridPane()
        this.children.add(test)
        this.alignment = Pos.CENTER
        var buttonsauveur = Button("je ne suis pas visible =)")
        buttonsauveur.isVisible=false
        buttonsauveur.padding = Insets(100.0,0.0,100.0,0.0)


        test.add(buttonsauveur,0,0)
        test.add(this.butonjouer,0,1)
        test.add(this.butonchargerunepartie,0,2)
        test.add(this.butonregles,0,3)
        test.vgap = 10.0

        butonjouer.setPrefSize(150.0,20.0)
        butonregles.setPrefSize(150.0,20.0)
        butonchargerunepartie.setPrefSize(150.0,20.0)

        //this.butonjouer.padding = Insets(4.0,42.5,4.0,42.5)
        //this.butonregles.padding = Insets(4.0,11.0,4.0,11.0)
        //this.butonchargerunepartie.padding = Insets(4.0,7.5,4.0,7.5)

        GridPane.setHalignment(this.butonregles, HPos.LEFT);
        test.alignment = Pos.CENTER

    }

    fun style (){
        this.styleClass.add("butonAccueil")
        this.styleClass.add("styleAccueil")
        this.butonjouer.styleClass.add("hoverAccueil")
        this.butonregles.styleClass.add("hoverAccueil")
        this.butonchargerunepartie.styleClass.add("hoverAccueil")


    }



}