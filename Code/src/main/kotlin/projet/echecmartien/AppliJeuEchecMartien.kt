package projet.echecmartien

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventType
import javafx.scene.Cursor
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.Dialog
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.stage.Stage
import projet.echecmartien.controleur.*
import projet.echecmartien.controleur.ControleurChangeScene
import projet.echecmartien.modele.IA
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.Joueur
import projet.echecmartien.vue.*
import java.lang.NumberFormatException


class AppliJeuEchecMartien: Application() {
    override fun start(primaryStage: Stage) {

        var vueAcceuil = Acceuil()
        var vueJeu = Jeux()
        var vueChoixParam = Choixparametres()
        var vueChoixSave = ChoixSave()
        var vueReglesdujeux = Reglesdujeux()
        var vueReglesdujeux2 = Reglesdujeux()
        val dialog = Alert(Alert.AlertType.INFORMATION)
        var ok: ButtonType = ButtonType("OK")
        dialog.buttonTypes.setAll(ok)
        val HOVERED_BUTTON_STYLE = "-fx-background-color: red;"


        var jeu = Jeu()





        var sceneAcceuil = Scene(vueAcceuil,1920.0/2, 1280.0/2)
        var sceneJeu = Scene(vueJeu, 1920.0/2, 1280.0/2)
        var sceneChoixParam = Scene(vueChoixParam, 1920.0/2, 1280.0/2)
        var sceneChoixSave = Scene(vueChoixSave, 1920.0/2, 1280.0/2)
        var sceneReglesdujeux = Scene(vueReglesdujeux, 1920.0/2, 1280.0/2)
        var sceneReglesdujeux2 = Scene(vueReglesdujeux2, 1920.0/2, 1280.0/2)


        sceneJeu.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
        sceneAcceuil.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
        sceneChoixParam.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
        sceneChoixSave.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
        sceneReglesdujeux.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())
        sceneReglesdujeux2.stylesheets.add(AppliJeuEchecMartien::class.java.getResource("vue/style.css")?.toExternalForm())


        //vueacceuil.butonregles.onAction = EventHandler{precedent = sceneAcceuil}
        vueAcceuil.butonregles.onAction = ControleurChangeScene(sceneReglesdujeux,primaryStage)
        vueAcceuil.butonchargerunepartie.onAction = ControleurChangeScene(sceneChoixSave,primaryStage)
        vueAcceuil.butonjouer.onAction = ControleurChangeScene(sceneChoixParam,primaryStage)

        vueChoixParam.nbjoueur1.addEventHandler(ActionEvent.ACTION, ControleurSurnom(vueChoixParam))
        vueChoixParam.nbjoueur2.addEventHandler(ActionEvent.ACTION, ControleurSurnom(vueChoixParam))

        vueAcceuil.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHover(vueAcceuil))

        vueAcceuil.butonjouer.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHover(vueAcceuil))
        vueAcceuil.butonjouer.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHover(vueAcceuil))

        vueAcceuil.butonregles.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHover(vueAcceuil))
        vueAcceuil.butonregles.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHover(vueAcceuil))

        vueAcceuil.butonchargerunepartie.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHover(vueAcceuil))
        vueAcceuil.butonchargerunepartie.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHover(vueAcceuil))

        vueChoixSave.button.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverChoixSave(vueChoixSave))
        vueChoixSave.button.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverChoixSave(vueChoixSave))

        vueChoixSave.buttonValider.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverChoixSave(vueChoixSave))
        vueChoixSave.buttonValider.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverChoixSave(vueChoixSave))

        vueReglesdujeux.retour.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverRegle(vueReglesdujeux))
        vueReglesdujeux.retour.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverRegle(vueReglesdujeux))

        vueReglesdujeux2.retour.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverRegle(vueReglesdujeux2))
        vueReglesdujeux2.retour.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverRegle(vueReglesdujeux2))

        vueReglesdujeux2.retour.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverRegle(vueReglesdujeux2))
        vueReglesdujeux2.retour.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverRegle(vueReglesdujeux2))

        vueJeu.buttonQuit.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverJeu(vueJeu))
        vueJeu.buttonQuit.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverJeu(vueJeu))

        vueJeu.buttonRegle.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverJeu(vueJeu))
        vueJeu.buttonRegle.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverJeu(vueJeu))

        vueChoixParam.buttonvalidee.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverChoixParam(vueChoixParam))
        vueChoixParam.buttonvalidee.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverChoixParam(vueChoixParam))

        vueChoixParam.buttonquitter.addEventHandler(MouseEvent.MOUSE_ENTERED, ControleurHoverChoixParam(vueChoixParam))
        vueChoixParam.buttonquitter.addEventHandler(MouseEvent.MOUSE_EXITED, ControleurHoverChoixParam(vueChoixParam))





        vueChoixSave.button.onAction = ControleurChangeScene(sceneAcceuil,primaryStage)
        vueJeu.buttonRegle.onAction = ControleurChangeScene(sceneReglesdujeux2,primaryStage)
        vueReglesdujeux2.retour.onAction = ControleurChangeScene(sceneJeu,primaryStage)
        vueReglesdujeux.retour.onAction = ControleurChangeScene(sceneAcceuil,primaryStage)
        vueChoixParam.buttonquitter.onAction = ControleurChangeScene(sceneAcceuil,primaryStage)

        //vueChoixParam.buttonvalidee.addEventHandler(ActionEvent.ACTION,ControleurChangeScene(sceneJeu,primaryStage))

        dialog.dialogPane.addEventHandler(ActionEvent.ACTION, ControleurChangeScene(sceneAcceuil, primaryStage))



        vueJeu.buttonQuit.addEventHandler(ActionEvent.ACTION) {

            if (vueJeu.chexboxsave.isSelected) {
                jeu.exportsave()
                //vueChoixSave.update()
                //si select save dans choix save et quitter en save overyde la save
            }
            vueJeu.buttonQuit.addEventHandler(ActionEvent.ACTION, ControleurChangeScene(sceneAcceuil, primaryStage))
        }

        vueChoixParam.buttonvalidee.addEventHandler(ActionEvent.ACTION) {
            try{
                var labelJ1 = ""
                var labelJ2 = ""
                var Nb = 0

            labelJ1 = vueChoixParam.textfildPseudo1.text
            labelJ2 = vueChoixParam.textfildPseudo2.text
            Nb = vueChoixParam.textfildNbcoupmax.text.toInt()
            println(Nb)

            val a = Joueur(labelJ1)
            val b:Joueur
            if (vueChoixParam.nbjoueur1.isSelected){
                b = IA(jeu)
            }else{
                b=Joueur(labelJ2)
            }

            jeu.initialiserPartie(a, b, Nb)
                labelJ1 = vueChoixParam.textfildPseudo1.text
                labelJ2 = vueChoixParam.textfildPseudo2.text
                Nb = vueChoixParam.textfildNbcoupmax.text.toInt()
                println(Nb)

                jeu.initialiserPartie(a, b, Nb)

                vueJeu.labelJ1.text = labelJ1
                vueJeu.labelJ2.text = labelJ2

                ControleurJeuMain(jeu,vueJeu).updateVue()

                println(jeu.getJoueurCourant())
                println(jeu.getJoueur1())
                println(a.getPseudo())
                println(jeu.getJoueur2())
                println(b.getPseudo())
                //println(jeu.deplacer(2, 2, 3, 3))
                println("nombre de coups sans prise " + jeu.getNombreSansprise())
                //println(jeu.joueurVainqueur())
                vueChoixParam.buttonvalidee.addEventHandler(ActionEvent.ACTION,ControleurChangeScene(sceneJeu,primaryStage))

            }
            catch (e:NumberFormatException){
                println("mauvais format")
            }

        }

//        if (!vueChoixParam.textfildPseudo1.text.isEmpty()){
//            vueChoixParam.buttonvalidee.addEventHandler(ActionEvent.ACTION,ControleurChangeScene(sceneJeu,primaryStage))
//        }
//        if (!vueChoixParam.textfildPseudo1.text.isEmpty() && vueChoixParam.nbjoueur1.isSelected && !vueChoixParam.textfildNbcoupmax.text.isEmpty()){
//            vueChoixParam.buttonvalidee.addEventHandler(ActionEvent.ACTION,ControleurChangeScene(sceneJeu,primaryStage))
//        }

        vueJeu.scene.addEventHandler(ActionEvent.ACTION) {
            if (jeu.arretPartie()) {
                println(jeu.getJoueurCourant().toString())
            }
        }

        vueJeu.eventAddCase(ControleurJeuMain(jeu,vueJeu))

        vueAcceuil.butonchargerunepartie.addEventHandler(ActionEvent.ACTION,ControleurSaveUpdate(vueChoixSave,jeu))

        sceneJeu.addEventHandler(MouseEvent.MOUSE_CLICKED) {
            dialog.title="Fin de la partie"
            dialog.headerText=" Le joueur vainqueur est : \n"
            if (jeu.joueurVainqueur() == null) {
                dialog.contentText = "Personne !!... Et oui vous êtes tous les deux à égalité"
            }
            else{
                dialog.contentText = jeu.joueurVainqueur()!!.getPseudo() + " bien joué"
            }

            if (jeu.arretPartie()){
                dialog.showAndWait()
            }
        }

        vueChoixSave.buttonValider.addEventHandler(ActionEvent.ACTION, controleurButtonValider(vueChoixSave,jeu,vueJeu,primaryStage,sceneJeu,vueChoixParam))

        vueReglesdujeux2.style()
        vueChoixParam.style()
        vueJeu.style()
        vueChoixSave.style()
        vueAcceuil.style()
        vueReglesdujeux.style()


        primaryStage.title="Echecs Martien"
        primaryStage.scene=sceneAcceuil


        primaryStage.show()

    }

}

fun main(){



    Application.launch(AppliJeuEchecMartien::class.java)

}