package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Acceuil
import projet.echecmartien.vue.ChoixSave
import projet.echecmartien.vue.Choixparametres
import projet.echecmartien.vue.Reglesdujeux

class ControleurHoverRegle(vue : Reglesdujeux) : EventHandler<MouseEvent> {
    var vue : Reglesdujeux

    init {
        this.vue = vue
    }
    override fun handle(p0: MouseEvent) {


        if (vue.retour.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.retour.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.retour.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }
    }
}