package projet.echecmartien.controleur

import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.geometry.Pos
import javafx.geometry.VPos
import javafx.scene.input.MouseEvent
import javafx.scene.layout.GridPane
import javafx.scene.layout.Pane
import javafx.scene.layout.StackPane
import javafx.scene.shape.Circle
import projet.echecmartien.exceptions.DeplacementException
import projet.echecmartien.modele.*
import projet.echecmartien.vue.Jeux

class ControleurJeuMain(modele : Jeu, vue : Jeux) : EventHandler<MouseEvent>{
    var modele : Jeu
    var vue : Jeux
    var coOrigine : Coordonnee?
    var coDestination : Coordonnee?
    var listpanecolore : MutableList<Pane>
    var liststylepaneclore : MutableList<String>

    init{
        this.listpanecolore = mutableListOf()
        this.liststylepaneclore = mutableListOf()

        this.coOrigine = null
        this.coDestination = null
        this.vue = vue
        this.modele = modele
    }

    override fun handle(event: MouseEvent) {
        //modele.initialiserPartie(Joueur("1"), Joueur("test"),4)


        val source = event.source
        var index = -1
        var index2 = -1
        if (source is StackPane){
            index = GridPane.getRowIndex(source)
            index2 = GridPane.getColumnIndex(source)
        }



        //MODELE PROBLEME : (Explication pas sur a 100%) a revoir re test corrigé !!
        //   Probleme exeption dans regle mal géré actuellement pion qui arrive dans zone nest pas du tout deplaçable devrait etre deplacable dans zone pour un tour puis partout
        //   Probleme passage au dessus d'un pion marche mais devrais pas
        //   Probleme on peut manger nos propre pions

        println("pion selectionné")
        if (coOrigine == null){ //probleme
            coOrigine = Coordonnee(index2,index)
            coDestination = null

        }else{

            coDestination = Coordonnee(index2,index)
            //print(coOrigine)
            if (coOrigine != null && coDestination != null){
                try{
                    println("${coOrigine!!.getX()},${coOrigine!!.getY()},${coDestination!!.getX()},${coDestination!!.getY()}")

                    modele.deplacer(coOrigine!!.getX(),coOrigine!!.getY(),coDestination!!.getX(),coDestination!!.getY())


                }
                catch (e : DeplacementException){
                    //ShowWarningalert()
                    println("non tu peux pas")
                    coDestination = null
                    coOrigine = null
                }
            }
            coOrigine = null
        }






        //vue update nb pion capturés :
        var countNbGrand1 = 0
        var countNbMoyen1 = 0
        var countNbPetit1 = 0
        var countNbGrand2 = 0
        var countNbMoyen2 = 0
        var countNbPetit2 = 0


        for (pion in modele.getJoueur1()!!.getPionsCaptures()){
            if (pion is GrandPion && pion !is MoyenPion){
                countNbGrand1++
            }
            if (pion is GrandPion && pion is MoyenPion){
                countNbMoyen1++
            }
            if (pion is PetitPion){
                countNbPetit1++
            }
        }
        for (pion in modele.getJoueur2()!!.getPionsCaptures()){
            if (pion is GrandPion && pion !is MoyenPion){
                countNbGrand2++
            }
            if (pion is GrandPion && pion is MoyenPion){
                countNbMoyen2++
            }
            if (pion is PetitPion){
                countNbPetit2++
            }
        }

        vue.labelNombreGrandJ1.text = countNbGrand2.toString()
        vue.labelNombreMoyenJ1.text = countNbMoyen2.toString()
        vue.labelNombrePetitJ1.text = countNbPetit2.toString()
        vue.labelNombreGrandJ2.text = countNbGrand1.toString()
        vue.labelNombreMoyenJ2.text = countNbMoyen1.toString()
        vue.labelNombrePetitJ2.text = countNbPetit1.toString()


        updateVue()
    }

    fun updateVue(){
        for (i in 0 until listpanecolore.size){
            listpanecolore[i].style = liststylepaneclore[i]
        }

        for (lignecollone in 0 until modele.getPlateau().getCases().size){ // 8

            for (case in 0 until modele.getPlateau().getCases()[lignecollone].size) {   //4
                if (coOrigine != null && coDestination == null){
                    println("$coOrigine,$coDestination")
                    try {
                        //println(modele.deplacementPossible(coOrigine!!.getX(),coOrigine!!.getY(),case,lignecollone,modele.getJoueurCourant()))

                        if (modele.deplacementPossible(coOrigine!!.getX(),coOrigine!!.getY(),case,lignecollone,modele.getJoueurCourant())){
                            this.listpanecolore.add(vue.getPane(case,lignecollone))
                            this.liststylepaneclore.add(vue.getPane(case,lignecollone).style)


                            vue.getPane(case,lignecollone).style = "-fx-background-color: YELLOW ;"+"-fx-opacity: 0.8"

                        }
                    }catch (e : DeplacementException){
                        println("wtf frero")
                    }

//                    if (modele.deplacementPossible(coOrigine!!.getX(),coOrigine!!.getY(),lignecollone,case,modele.getJoueurCourant())){
//                       // vue.getPane(case,lignecollone).style = "-fx-background-color: YELLOW ;"+"-fx-opacity: 0.8"
//                        print("a")
//                    }
                }else{
                    //vue.getPane(case,lignecollone).style = "-fx-background-color: YELLOW ;"+"-fx-opacity: 0.8"
                }


                if (modele.getPlateau().getCases()[lignecollone][case].estLibre()) { // est libre est inversé

                    if (modele.getPlateau().getCases()[lignecollone][case].getPion() is GrandPion && modele.getPlateau().getCases()[lignecollone][case].getPion() !is MoyenPion){
                        vue.getPane(case,lignecollone).children.clear()
                        var newcercle = Circle(23.0)
                        newcercle.setFill(javafx.scene.paint.Color.BLACK)
                        //print("ici")
                        vue.getPane(case,lignecollone).children.add(newcercle)


                    }
                    if (modele.getPlateau().getCases()[lignecollone][case].getPion() is MoyenPion && modele.getPlateau().getCases()[lignecollone][case].getPion()  is GrandPion){
                        //print("ici")
                        vue.getPane(case,lignecollone).children.clear()
                        var newcerclemoy = Circle(17.0)
                        newcerclemoy.setFill(javafx.scene.paint.Color.BLACK)

                        vue.getPane(case,lignecollone).children.add(newcerclemoy)

                    }
                    if (modele.getPlateau().getCases()[lignecollone][case].getPion() is PetitPion){
                        vue.getPane(case,lignecollone).children.clear()
                        var necerclepetit = Circle(12.0)
                        necerclepetit.setFill(javafx.scene.paint.Color.BLACK)
                        vue.getPane(case,lignecollone).children.add(necerclepetit)




                    }


                }else{
                    vue.getPane(case,lignecollone).children.clear()
                }
            }



        }
        if (modele.getJoueurCourant()==modele.getJoueur1()){
            vue.labelJ1.style+=("-fx-border-width: 1 ;"+"-fx-border-color: white ;")
            vue.labelJ2.style=("-fx-text-fill : #5766c9 ;"+"-fx-font-size : 20.0; "+ "-fx-font-weight : BOLD; ")
        }
        else {
            vue.labelJ2.style += ("-fx-border-width: 1 ;" + "-fx-border-color: white ;")
            vue.labelJ1.style=("-fx-text-fill : #b34253 ;"+"-fx-font-size : 20.0; "+ "-fx-font-weight : BOLD; ")
        }
    }


}




