package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Acceuil
import projet.echecmartien.vue.ChoixSave

class ControleurHover(vue : Acceuil) : EventHandler<MouseEvent> {
    var vue : Acceuil

    init {
        this.vue = vue
    }
    override fun handle(p0: MouseEvent) {


        if (vue.butonjouer.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.butonjouer.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.butonjouer.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

        if (vue.butonregles.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.butonregles.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.butonregles.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

        if (vue.butonchargerunepartie.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.butonchargerunepartie.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.butonchargerunepartie.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }
    }
}