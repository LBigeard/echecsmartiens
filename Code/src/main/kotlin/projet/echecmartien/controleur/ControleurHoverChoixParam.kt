package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Acceuil
import projet.echecmartien.vue.ChoixSave
import projet.echecmartien.vue.Choixparametres

class ControleurHoverChoixParam(vue : Choixparametres) : EventHandler<MouseEvent> {
    var vue : Choixparametres

    init {
        this.vue = vue
    }
    override fun handle(p0: MouseEvent) {


        if (vue.buttonvalidee.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.buttonvalidee.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.buttonvalidee.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

        if (vue.buttonquitter.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.buttonquitter.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.buttonquitter.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }
    }
}