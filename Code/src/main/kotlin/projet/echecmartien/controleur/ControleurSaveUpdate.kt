package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.getNameofSave
import projet.echecmartien.vue.ChoixSave

class ControleurSaveUpdate(vue : ChoixSave,modele : Jeu) : EventHandler<ActionEvent>{
    var modele : Jeu
    var vue : ChoixSave
    init{
        this.modele = modele
        this.vue = vue
    }

    override fun handle(p0: ActionEvent?) {

        var listesave = getNameofSave().getliseofname().toList()
        vue.update(listesave)


        vue.gridPane.children.forEach { it.onMouseClicked =  controleurlistesave(vue)}


    }


}