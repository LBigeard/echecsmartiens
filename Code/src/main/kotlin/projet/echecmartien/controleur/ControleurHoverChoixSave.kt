package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Acceuil
import projet.echecmartien.vue.ChoixSave

class ControleurHoverChoixSave(vue : ChoixSave) : EventHandler<MouseEvent> {
    var vue : ChoixSave

    init {
        this.vue = vue
    }
    override fun handle(p0: MouseEvent) {


        if (vue.button.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.button.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.button.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

        if (vue.buttonValider.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.buttonValider.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.buttonValider.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }
    }
}