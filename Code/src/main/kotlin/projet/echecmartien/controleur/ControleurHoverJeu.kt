package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Acceuil
import projet.echecmartien.vue.ChoixSave
import projet.echecmartien.vue.Jeux

class ControleurHoverJeu(vue : Jeux) : EventHandler<MouseEvent> {
    var vue : Jeux

    init {
        this.vue = vue
    }
    override fun handle(p0: MouseEvent) {


        if (vue.buttonQuit.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.buttonQuit.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.buttonQuit.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

        if (vue.buttonRegle.isHover && p0.eventType  == MouseEvent.MOUSE_ENTERED){
            vue.buttonRegle.style = ( "-fx-background-color: yellow;")
        }
        if (p0.eventType == MouseEvent.MOUSE_EXITED){
            vue.buttonRegle.style =("-fx-background-color:#f9f9e3;"+"-fx-opacity: 0.8;")
        }

    }
}