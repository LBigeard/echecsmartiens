package projet.echecmartien.controleur

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import projet.echecmartien.AppliJeuEchecMartien
import projet.echecmartien.vue.Choixparametres
import projet.echecmartien.vue.Jeux

class ControleurSurnom(vue : Choixparametres) : EventHandler<ActionEvent> {
    var vue : Choixparametres

    init {
        this.vue = vue
    }

    override fun handle(p0: ActionEvent) {
        if (vue.nbjoueur1.isSelected){
            vue.textfildPseudo2.visibleProperty().set(false)
        }

        if (vue.nbjoueur2.isSelected){
            vue.textfildPseudo2.visibleProperty().set(true)
        }
    }
}