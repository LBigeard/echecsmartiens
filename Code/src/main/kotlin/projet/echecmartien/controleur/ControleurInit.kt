package projet.echecmartien.controleur

import javafx.event.ActionEvent
import javafx.event.EventHandler
import projet.echecmartien.modele.Jeu
import projet.echecmartien.modele.Joueur
import projet.echecmartien.vue.Choixparametres
import projet.echecmartien.vue.Jeux

class ControleurInit(vue : Choixparametres, jeu : Jeu, vueJeu : Jeux) : EventHandler<ActionEvent> {


    var vue : Choixparametres
    var jeu : Jeu
    var vueJeu : Jeux

    init {
        this.vue = vue
        this.vueJeu = vueJeu
        this.jeu = jeu
    }

    override fun handle(p0: ActionEvent?) {

        var labelJ1 = ""
        var labelJ2 = ""
        var Nb = 0

        labelJ1 = vue.textfildPseudo1.text
        labelJ2 = vue.textfildPseudo2.text
        Nb = vue.textfildNbcoupmax.text.toInt()
        println(Nb)
        val a = Joueur(labelJ1)
        val b = Joueur(labelJ2)
        jeu.initialiserPartie(a, b, Nb)

        vueJeu.labelJ1.text = labelJ1
        vueJeu.labelJ2.text = labelJ2

        println(jeu.getJoueurCourant())
        println(jeu.getJoueur1())
        println(a.getPseudo())
        println(jeu.getJoueur2())
        println(b.getPseudo())
        println(jeu.deplacer(2, 2, 3, 3))
        println("nombre de coups sans prise " + jeu.getNombreSansprise())
        println(jeu.deplacer(2, 5, 1, 4))
        println(jeu.joueurVainqueur())
    }

}