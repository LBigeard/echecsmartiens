module projet.echecmartien {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;

    requires org.controlsfx.controls;
    requires javafx.graphics;
    requires com.google.gson;
    requires java.base;

    opens projet.echecmartien to javafx.fxml;
    exports projet.echecmartien;
    opens projet.echecmartien.modele to com.google.gson;
    exports projet.echecmartien.modele;

    opens projet.echecmartien.librairie to com.google.gson;
    exports projet.echecmartien.librairie;

}